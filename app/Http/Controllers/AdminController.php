<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Validator;
use App\Models\User;
use App\Models\Tour;
use App\Models\TourPaxes;
use App\Models\TourPeak;
use App\Models\Activity;
use App\Models\Agent;
use App\Models\AgentContact;
use Carbon;
use Auth;
use DB;
use Alert;
use Session;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $activities = Activity::where('user', Auth::user()->name)->orderBy('created_at', 'desc')->simplePaginate(10);

        return view('pages.admin.admin_dashboard', compact('activities'));

    }

    /**
     * Retrieve users and levels in user page.
     *
     * @return \Illuminate\Http\Response
     */

    public function getUsers()
    {

        $users = User::all();

        return view('pages.admin.users.users', compact('users'));

    }

    /**
     * Get user in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAddUser()
    {

        return view('pages.admin.users.add_user');

    }

    /**
     * Get user by id in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function getEditUser($id)
    {

        $user = User::where('id', $id)->get();

        return view('pages.admin.users.edit_user', compact('user'));

    }

    /**
     * Update user by id in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function postEditUser($id, Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'username' => 'required|string|max:255',
            'level' => 'required|string',
            'status' => 'required|string'
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        } else {
            $input = $request->all();

            $user = User::find($id);

            $user->name = $input['name'];
            $user->username = $input['username'];
            $user->level = $input['level'];
            $user->status = $input['status'];
            $user->save();

            $this->insertActivity('updated', 'user', $input['name']);

            return redirect('admin/users');
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function postAddUser(Request $request)
    {

        $input = $request->all();

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
            'username' => 'required|string|max:255',
            'level' => 'required|string',
            'status' => 'required|string'
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        } else {

            $password = bcrypt($input['password']);

            $user = new User;

            $user->name = $input['name'];
            $user->username = $input['username'];
            $user->email = $input['email'];
            $user->password = $password;
            $user->level = $input['level'];
            $user->status = $input['status'];
            $user->save();

            $this->insertActivity('added', 'users', $input['name']);

            return redirect('/admin/users');
        }
    }

    /**
     * Get all tours
     *
     * @return \Illuminate\Http\Response
     */

    public function getTours()
    {

        $tours = Tour::all();
        $codes = Tour::all()->groupBy('code')->pluck('code');

        return view('pages.admin.tour_manager.tour.tours', compact('tours', 'codes'));
    }

    /**
     * Get tour in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAddTour()
    {
        return view('pages.admin.tour_manager.tour.add_tour');
    }

    public function postAddTour(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'type' => 'required',
            'code' => 'required',
            'duration' => 'required',
            'lead_time' => 'required',
            'min_pax' => 'required',
            'foreign_rate' => 'required',
            'pickup' => 'required',
            'description' => 'required',
            'inclusions' => 'required',
            'highlights' => 'required',
            'w_amount.*' => 'required',
            'wo_amount.*' => 'required',
            'peak_from' => 'required',
            'peak_to' => 'required',
            'peak_type' => 'required',
            'peak_amount' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        } else {
            $input = $request->all();

            $tour = new Tour;

            $tour->name = $input['name'];
            $tour->type = $input['type'];
            $tour->code = $input['code'];
            $tour->duration = $input['duration'];
            $tour->lead_time = $input['lead_time'];
            $tour->min_pax = $input['min_pax'];
            $tour->foreign_rate = $input['foreign_rate'];
            $tour->description = $input['description'];
            $tour->inclusions = $input['inclusions'];
            $tour->highlights = $input['highlights'];
            $tour->pick_up = $input['pickup'];
            $tour->status = 1;
            $tour->save();

            $this->insertTourPeak($tour->id, $input['peak_from'], $input['peak_to'], $input['peak_type'], $input['peak_amount']);

            for ($i = 1; $i <= 12; $i++) {
                $t = new TourPaxes;
                $t->tour = $tour->id;
                $t->with_guide = $request->w_amount[$i];
                $t->without_guide = $request->wo_amount[$i];
                $t->pax = $i;
                $t->save();
            }

            return redirect('admin/tours');
        }
    }

    public function getEditTour($id)
    {
        $tour = Tour::where('id', $id)->first();
        $tour_paxes = TourPaxes::where('tour', $id)->orderBy('pax')->get();
        $tour_peak = TourPeak::where('tour', $id)->first();

        return view('pages.admin.tour_manager.tour.edit_tour', compact('tour', 'tour_paxes', 'tour_peak'));

    }

    public function postEditTour($id, Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'type' => 'required',
            'code' => 'required',
            'duration' => 'required',
            'lead_time' => 'required',
            'min_pax' => 'required',
            'foreign_rate' => 'required',
            'pickup' => 'required',
            'description' => 'required',
            'inclusions' => 'required',
            'highlights' => 'required',
            'w_amount.*' => 'required',
            'wo_amount.*' => 'required',
            'peak_from' => 'required',
            'peak_to' => 'required',
            'peak_type' => 'required',
            'peak_amount' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        } else {
            $input = $request->all();

            $tour = Tour::where('id', $id)->first();
            $tour->id = $id;
            $tour->name = $input['name'];
            $tour->type = $input['type'];
            $tour->code = $input['code'];
            $tour->duration = $input['duration'];
            $tour->lead_time = $input['lead_time'];
            $tour->min_pax = $input['min_pax'];
            $tour->foreign_rate = $input['foreign_rate'];
            $tour->description = $input['description'];
            $tour->inclusions = $input['inclusions'];
            $tour->highlights = $input['highlights'];
            $tour->pick_up = $input['pickup'];
            $tour->status = 1;
            $tour->update();

            $this->insertTourPeak($tour->id, $input['peak_from'], $input['peak_to'], $input['peak_type'], $input['peak_amount']);

            // recreate tour paxes
            TourPaxes::where('tour', $tour->id)->delete();
            for ($i = 1; $i <= 12; $i++) {
                $t = new TourPaxes;
                $t->tour = $tour->id;
                $t->with_guide = $request->w_amount[$i];
                $t->without_guide = $request->wo_amount[$i];
                $t->pax = $i;
                $t->save();
            }

            return redirect('admin/tours');
        }

    }

    public function getAgents()
    {
        $agents = Agent::join('agents_contact', 'agents.id', '=', 'agents_contact.agent')
            ->select('agents.id', 'agents.name', 'agents.nature', DB::raw('count(agents_contact.agent) as aCount'))
            ->groupBy('agents.id', 'agents.name', 'agents.nature')
            ->where('agents.status', 0)->simplePaginate(16);
        $natures = Agent::where('status', 0)->groupBy('nature')->pluck('nature');

        return view('pages.admin.tour_manager.agents.agents', compact('agents', 'natures'));

    }

    public function getAddAgent()
    {

        return view('pages.admin.tour_manager.agents.add_agent');
    }

    public function postAddAgent(Request $request)
    {
        $validator = Validator::make($request->all(), [

            'name' => 'required',
            'address' => 'required',
            'nature' => 'required',
            'tin' => 'required',
            'contract_rate' => 'required',
            'payment_terms' => 'required',
            'notes' => 'required',
            'contact_name' => 'required',
            'contact_designation' => 'required',
            'contact_email' => 'required',
            'contact_number' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $input = $request->all();

        $agent = new Agent;
        $agent->name = $input['name'];
        $agent->nature = $input['nature'];
        $agent->address = $input['address'];
        $agent->tin = $input['tin'];
        $agent->notes = $input['notes'];
        $agent->contract_rate = $input['contract_rate'];
        $agent->payment_terms = $input['payment_terms'];
        $agent->status = 0;
        $agent->save();

        $this->insertActivity('added', 'agents', $input['name']);
        $this->insertAgentContact($agent->id, $input['contact_name'], $input['contact_designation'], $input['contact_email'], $input['contact_number'], $agent->name);

        return redirect('admin/agents');
    }

    public function getEditAgent($id, Request $request)
    {
        $agent = Agent::where('id', $id)->first();
        $contact = AgentContact::where('agent', $id)->where('status', 0)->first();

        return view('pages.admin.tour_manager.agents.edit_agent', compact('agent', 'contact'));
    }

    public function postEditAgent($id, Request $request)
    {
        $validator = Validator::make($request->all(), [

            'name' => 'required',
            'address' => 'required',
            'nature' => 'required',
            'tin' => 'required',
            'contract_rate' => 'required',
            'payment_terms' => 'required',
            'notes' => 'required',
            'contact_name' => 'required',
            'contact_designation' => 'required',
            'contact_email' => 'required',
            'contact_number' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        } else {

            $input = $request->all();

            $agent = Agent::find($id);
            $agent->name = $input['name'];
            $agent->nature = $input['nature'];
            $agent->address = $input['address'];
            $agent->tin = $input['tin'];
            $agent->contract_rate = $input['contract_rate'];
            $agent->payment_terms = $input['payment_terms'];
            $agent->notes = $input['notes'];
            $agent->status = 0;
            $agent->save();

            $this->insertActivity('updated', 'agents', $input['name']);
            $this->insertAgentContact($agent->id, $input['contact_name'], $input['contact_designation'], $input['contact_email'], $input['contact_number']);

            return redirect('admin/agents');
        }

    }

    public function insertAgentContact($agent, $name, $designation, $email, $contact)
    {
        $a = AgentContact::firstOrNew(['id' => $agent]);

        $a->agent = $agent;
        $a->name = $name;
        $a->designation = $designation;
        $a->email = $email;
        $a->contact_number = $contact;
        $a->status = 0;
        $a->save();

    }

    public function insertTourPax($tour, $pax, $with_guide, $without_guide, $pId)
    {

        foreach ($pax as $k => $v) {

            $i = TourPaxes::firstOrNew(['id' => $pId[$k]], ['pax' => $pax[$k]]);
            $i->tour = $tour;
            $i->with_guide = $with_guide[$k];
            $i->without_guide = $without_guide[$k];
            $i->pax = $pax[$k];
            $i->save();

        }

    }

    public function insertTourPeak($tour, $from, $to, $type, $amount)
    {
        if ($from != NULL || $to != NULL) {
            $i = TourPeak::firstOrNew(['from' => $from], ['to' => $to]);
            $i->tour = $tour;
            $i->from = $from;
            $i->to = $to;
            $i->type = $type;
            $i->amount = $amount;
            $i->save();
        }

    }

    public function insertActivity($activity, $subject, $content)
    {

        $act = new Activity;
        $act->user = Auth::user()->name;
        $act->activity = $activity;
        $act->content = $content;
        $act->subject = $subject;
        $act->save();

    }

    public function getFilterSearch(Request $request)
    {

        $act = $request->input('act');
        $var = $request->input('type');
        $fs = $request->input('fs');

        if ($var === "tours") {

            if ($act === "search") {
                $result = Tour::where('name', 'like', '%' . $fs . '%')->where('status', 0)->get();
            } else {
                $result = Tour::where('code', $fs)->where('status', 0)->get();
            }

            return view('pages.ajax.tours_result', compact('result'));

        } elseif ($var === "agents") {

            if ($act === "search") {
                //$result = Agent::where('name','like','%'.$fs.'%')->where('status',0)->get();
                $result = Agent::join('agents_contact', 'agents.id', '=', 'agents_contact.agent')
                    ->select('agents.id', 'agents.name', 'agents.nature', DB::raw('count(agents_contact.agent) as aCount'))
                    ->groupBy('agents.id', 'agents.name', 'agents.nature')
                    ->where('agents.name', 'like', '%' . $fs . '%')
                    ->where('agents.status', 0)
                    ->get();
            } else {
                $result = Agent::join('agents_contact', 'agents.id', '=', 'agents_contact.agent')
                    ->select('agents.id', 'agents.name', 'agents.nature', DB::raw('count(agents_contact.agent) as aCount'))
                    ->groupBy('agents.id', 'agents.name', 'agents.nature')
                    ->where('agents.nature', $fs)
                    ->where('agents.status', 0)
                    ->get();
            }

            return view('pages.ajax.agents_result', compact('result'));

        } elseif ($var === "users") {

            if ($act === "search") {
                $result = User::where('name', 'like', '%' . $fs . '%')->where('status', 0)->get();
            } else {
                if ($fs === "admin") {
                    $level = "isAdmin";
                } elseif ($fs === "reservation") {
                    $level = "isReservation";
                } elseif ($fs === "accounting") {
                    $level = "isAccounting";
                } else {
                    $level = "isAgent";
                }

                $result = User::where($level, $fs)->where('status', 0)->get();
            }

            $count = count($result);

            return view('pages.ajax.users_result', compact('result', 'count'));

        }

    }

    public function delete(Request $request)
    {

        $id = $request->input('id');
        $var = $request->input('var');
        $name = $request->input('name');

        if ($var === "tours") {
            $table = Tour::find($id);
        } elseif ($var === "agents") {
            $table = Agent::find($id);
        } elseif ($var === "user") {
            $table = User::find($id);
        }

        $table->status = 1;
        $table->save();

        $this->insertActivity('deleted', $var, $name);

        return "success";

    }

    public function deleteconorpeak(Request $request)
    {

        $id = $request->input('id');
        $check = $request->input('check');


        if ($check === "contact") {
            $ac = AgentContact::find($id);
            $ac->status = 1;
            $ac->save();
        } else {
            $peak = TourPeak::where('id', $id)->delete();
        }

        return "success";


    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
