<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Agent
 *
 * @mixin Builder
 */
class Agent extends Model
{
    protected $table = 'agents';

     protected $fillable = [
        'name',
        'nature',
        'address',
        'tin',
        'contract_rate',
        'payment_terms',
        'status',
    ];
}
