<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TourPeak
 *
 * @mixin Builder
 */
class TourPeak extends Model
{
    protected $table = 'tour_peak';

     protected $fillable = [
        'tour',
        'from',
        'to',
        'type',
        'amount',
    ];

    protected $dates = [
        'from',
        'to',
    ];
}
