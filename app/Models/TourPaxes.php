<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TourPaxes
 *
 * @mixin Builder
 */
class TourPaxes extends Model
{
    protected $table = 'tours_paxes';

     protected $fillable = [
        'tour',
        'with_guide',
        'without_guide',
        'pax',
        'amount',
    ];
}
