<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Tour
 *
 * @mixin Builder
 */
class Tour extends Model
{

    protected $table = 'tours';

     protected $fillable = [
        'name',
        'type',
        'code',
        'lead_time',
        'foreign_rate',
        'description',
        'inclusions',
        'highlights',
        'pickup',
    ];
}
