<?php

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\ReservationController;
use App\Http\Controllers\AccountingController;
use App\Models\Activity;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();
Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
	return view('dashboard');
})->name('dashboard');

Route::group(['middleware' => 'auth'], function () {
    Route::get('reservation/tours', [ReservationController::class, 'getTours']);
});

Route::middleware(['admin'])->group(function () {
    Route::prefix('admin')->group(function () {
		Route::get('/', [AdminController::class, 'index']);
        Route::get('user', [AdminController::class, 'getAddUser']);
		Route::post('add/user', [AdminController::class, 'postAddUser']);
        Route::get('edit/user/{id}', [AdminController::class, 'getEditUser']);
        Route::post('edit/user/{id}',[AdminController::class, 'postEditUser']);

		Route::get('tour', [AdminController::class, 'getAddTour']);
		Route::post('add/tour', [AdminController::class, 'postAddTour']);
		Route::get('edit/tour/{id}', [AdminController::class, 'getEditTour']);
		Route::post('edit/tour/{id}',[AdminController::class, 'postEditTour']);

		Route::get('agent', [AdminController::class, 'getAddAgent']);
		Route::post('add/agent', [AdminController::class, 'postAddAgent']);
		Route::get('edit/agent/{id}', [AdminController::class, 'getEditAgent']);
		Route::post('edit/agent/{id}', [AdminController::class, 'postEditAgent']);

		Route::post('delete', [AdminController::class, 'delete']);
		Route::post('deleteconorpeak', [AdminController::class, 'deleteconorpeak']);
		Route::get('tours', [AdminController::class, 'getTours']);
		Route::get('users', [AdminController::class, 'getUsers']);
		Route::get('agents', [AdminController::class, 'getAgents']);
		Route::get('tour-fs', [AdminController::class, '@getFilterSearch']);
	});
});

Route::middleware(['reservation'])->group(function () {
    Route::prefix('reservation')->group(function () {
        Route::get('/', [ReservationController::class, 'index']);
		Route::get('tours', [ReservationController::class, 'getTours']);
		Route::get('agents', [ReservationController::class, 'getAgents']);
		Route::get('salesinvoice', [ReservationController::class, 'getSalesInvoice']);
		Route::get('add/salesinvoice', [ReservationController::class, 'getAddSalesInvoice']);
		Route::post('add/salesinvoice', [ReservationController::class, 'postAddSalesInvoice']);

		Route::get('edit/salesinvoice/{id}', [ReservationController::class, 'getEditSalesInvoice']);
		Route::post('edit/salesinvoice/{id}', [ReservationController::class, 'postEditSalesInvoice']);
		Route::get('tour-fs', [AdminController::class, 'getFilterSearch']);
		Route::get('si-agent-search', [ReservationController::class, 'getSiAgentSearch']);
		Route::get('si-particular-search', [ReservationController::class, 'getSiParticularSearch']);
		Route::get('si-agent-add', [ReservationController::class, 'getSiAgentAdd']);
		Route::get('si-particular-add', [ReservationController::class, 'getSiParticularAdd']);
		Route::get('si-particular-calc', [ReservationController::class, 'getSiParticularCalc']);
		Route::get('si-search', [ReservationController::class, 'getSiSearch']);
		Route::get('si-filter-row', [ReservationController::class, 'getFilterSiRow']);
		Route::get('remove/particular-edit', [ReservationController::class, 'deleteParticularEdit']);
		Route::get('view/particular/{id}', [ReservationController::class, 'getViewParticular']);
		Route::get('view/agent/{id}', [ReservationController::class, 'getViewAgent']);
	});
});

Route::middleware(['accounting'])->group(function () {
    Route::prefix('accounting')->group(function () {
		Route::get('/', [AccountingController::class, 'index']);
		Route::get('statement-of-account', [AccountingController::class, 'getSalesInvoice']);
		Route::get('statement-of-account/verified', [AccountingController::class, 'getSalesInvoiceVerified']);
		Route::get('statement-of-account/table', [AccountingController::class, 'getSalesInvoiceTable']);
		Route::get('statement-of-account/table/verified', [AccountingController::class, 'getSalesInvoiceTableVerified']);

		Route::get('aging-of-accounts-receivable', [AccountingController::class, 'getAging']);
		Route::get('collectible-accounts', [AccountingController::class, 'getCollection']);
		Route::get('income-statements', [AccountingController::class, 'getIncomeStatement']);

		Route::get('sales-report', [AccountingController::class, 'getSalesReport']);
		Route::post('verify', [AccountingController::class, 'getVerifySoa']);
		Route::get('check-soa/{id}', [AccountingController::class, 'getCheckSi']);
		Route::post('check-soa/{id}', [AccountingController::class, 'postCheckSi']);
		Route::get('tours', [AccountingController::class, 'getTours']);
		Route::get('agents', [AccountingController::class, 'getAgents']);
		Route::get('tour-fs', [AdminController::class, 'getFilterSearch']);
		Route::get('view/particular/{id}', [AccountingController::class, 'getViewParticular']);
		Route::get('view/agent/{id}', [AccountingController::class, 'getViewAgent']);
		Route::get('si-search', [AccountingController::class, 'getSiSearch']);
		Route::get('si-search-table', [AccountingController::class, 'getSiSearchTable']);
		Route::get('si-filter-row', [AccountingController::class, 'getFilterSiRow']);
		Route::get('si-filter-table-row', [AccountingController::class, 'getFilterSiTableRow']);
		Route::get('sr-get-date', [AccountingController::class, 'getSalesReportDate']);
		Route::get('print/{id}', [AccountingController::class, 'getPrint']);
		Route::get('print-sales-report/{start}/{end}', [AccountingController::class, 'getPrintSalesReport']);
	});
});

/**/

