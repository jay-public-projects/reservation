<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_invoice', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sales_invoice');
            $table->string('reservation_officer');
            $table->string('booking_reference');
            $table->string('contact');
            $table->string('email');
            $table->string('lead_guest');
            $table->string('pick_up');
            $table->time('time');
            $table->string('note');
            $table->integer('total');
            $table->integer('vat');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_invoice');
    }
}
