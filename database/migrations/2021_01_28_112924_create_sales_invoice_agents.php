<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesInvoiceAgents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('sales_invoice_agent', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('si_id');
            $table->integer('agent_id');
            $table->string('name');
            $table->string('address');
            $table->string('tin');
            $table->integer('contract_rate');
            $table->string('payment_terms');
            $table->string('notes');
            $table->string('nature');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('sales_invoice_agent');
    }
}
