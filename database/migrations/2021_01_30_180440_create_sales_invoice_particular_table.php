<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesInvoiceParticularTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_invoice_particular', function (Blueprint $table) {
            $table->id();
            $table->integer('si_id');
            $table->string('lead_guest');
            $table->date('tour_date');
            $table->string('particular');
            $table->string('particular_id');
            $table->string('particular_type');
            $table->string('particular_guide');
            $table->integer('pax');
            $table->integer('foreign_pax');
            $table->integer('rate');
            $table->integer('foreign_rate');
            $table->integer('commission');
            $table->integer('total');
            $table->string('booking_reference');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_invoice_particular');
    }
}
