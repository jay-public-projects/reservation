<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
            'name' => 'admin',
            'username' => 'admin',
            'email' => 'admin@admin.com',
            'email_verified_at' =>now(),
            'password' => bcrypt('admin'),
            'level' => 0,
            'status' => 1,
            'remember_token' => Str::random(10),
            'created_at' => now(),
            'updated_at' => now()
        ]);
        // reservation
        DB::table('users')->insert([
            'name' => 'reservation',
            'username' => 'reservation',
            'email' => 'reservation@admin.com',
            'email_verified_at' =>now(),
            'password' => bcrypt('reservation'),
            'level' => 1,
            'status' => 1,
            'remember_token' =>  Str::random(10),
            'created_at' => now(),
            'updated_at' => now()
        ]);

        // accounting
        DB::table('users')->insert([
            'name' => 'accounting',
            'username' => 'accounting',
            'email' => 'accounting@admin.com',
            'email_verified_at' =>now(),
            'password' => bcrypt('accounting'),
            'level' => 2,
            'status' => 1,
            'remember_token' => Str::random(10),
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
