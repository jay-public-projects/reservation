# Reservation System

## Setup

1. Copy `.env.sample` to `.env`.
1. Change the DB connection config.
1. Generate the application key using `php artisan key:generate`.
1. Migrate the DB schema using `php artisan migrate`.
1. Setup the initial DB seeds using `php artisan db:seed`.
1. Start the development server using `php artisan serve`.
