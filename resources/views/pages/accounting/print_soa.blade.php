

<!DOCTYPE html>
<html>
<head>
    <title>Services Report</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <style>
			.logo-div{
				padding-bottom:30px;
			}
			.form-control{
				color:black !important;
				font-weight: bold !important;
				font-size:15px !important;
				height:50px !important;
				font-family:-webkit-pictograph !important;
			}
			
			.field-form::-webkit-input-placeholder { /* Chrome/Opera/Safari */
			color: black !important;
			font-weight:bold !important;
			font-size:15px;
			text-transform: uppercase !important;
			}
			.form-control-price::-webkit-input-placeholder { /* Chrome/Opera/Safari */
			color: black !important;
			font-weight:bold !important;
			font-size:10px !important;
			text-transform: uppercase !important;
			}
			.field-form::-moz-placeholder { /* Firefox 19+ */
				color: black !important;
				font-weight:bold !important;
				font-size:15px;
				text-transform: uppercase !important;
			}
			.field-form:-ms-input-placeholder { /* IE 10+ */
				color: black !important;
				font-weight:bold !important;
				font-size:15px;
				text-transform: uppercase !important;
			}
			.field-form:-moz-placeholder { /* Firefox 18- */
				color: black !important;
				font-weight:bold !important;
				font-size:15px;
				text-transform: uppercase !important;
			}
			
			.form-div{
				top: 60px;
			}
			h2{
				color:black;
				font-weight:bold;
			}
			.table-condensed>tbody>tr>td,
			.table-condensed>tbody>tr>th, 
			.table-condensed>tfoot>tr>td,
			.table-condensed>tfoot>tr>th{
				
			}
			.table-condensed>tbody>tr>td,
			.table-condensed>tbody>tr>th{
				vertical-align: middle;
			}
			.table>thead>tr>th {
				vertical-align: bottom;
				border-bottom: 2px solid #525252;
			}
			.table{
					background-color: #fff !important;
					border:1px solid black !important;
			}
			body{
				background:white !important;
			}
			.label-pax{
				font-size:9px;
			}
			.f11{
				font-size:12px;
			}
			.label-table{
				font-size:15px !important;
			}
			.table-no-border, .table-no-border>thead>tr>th, .table-no-border>tbody>tr>td{
				border:none !important;
				padding-top:8px;
				padding-bottom:8px;
			}
			.lower-case{
				text-transform: lowercase !important;
			}
			.padding-bottom-unset{
				padding-bottom:unset !important;
			}
			.soa-div{
				border:1px solid black;
				padding: 5px;
			}
			.text-transform-reset{
				text-transform: unset !important;
			}
		</style>
</head>
<body>
	<div class="row m-t-30">
				@foreach($si as $s)
						<div class="container-fluid">
							<div class="col-xs-12 form-div">
								<div class="soa-div">
									@php
										$_created = Carbon\Carbon::parse($s->created_at);
										$created = $_created->toDateString();
										$dispCreated = $_created->format('F d Y');
										$time = $_created->format('h:i:s A');
										$_pTime = Carbon\Carbon::parse($s->time);
										$pickup_time = $_pTime->format('h:i:s A');
										$total = 0;
										$net = 0;
									@endphp
									@foreach($sia as $agent)
										@php
											$agent_name = $agent->name;
											$agent_tin = $agent->tin;
											$agent_address = $agent->address;
										@endphp
									@endforeach
									
									<table>
										<thead>
											<tr>
												<td><span class="f12"><strong>SI #:</strong> {{$s->sales_invoice}}</span></td>
												<td><span class="f12"><strong>LEAD GUEST:</strong> {{$s->lead_guest}}</span></td>
												<td><span class="f12"><strong>EMAIL:</strong>{{$s->email}}</span> </td>
												<td><span class="f12"><strong>DATE:</strong>{{$dispCreated}}</span></td>
											</tr>
											<tr>
												<td><span class="f12"><strong>AGENT:</strong>{{$agent_name}}</span></td>
												<td><span class="f12"><strong>BOOKING REF#:</strong>{{$s->booking_reference}}</span></td>
												<td><span class="f12"><strong>CONTACT:</strong>{{$s->contact}}</span></td>
											</tr>
											<tr>
												<td><span class="f12"><strong>TIN:</strong> {{$agent_tin}}</span>&nbsp;</td>
											</tr>
											<tr>
												<td><span class="f12"><strong>ADDRESS:</strong>{{$agent_address}}</span>  </td>
											</tr>
										</thead>
									</table>&nbsp;
								
									<table class="table table-condensed">
										<thead>
											<tr>
												<th>Date</th>
												<th>Particular</span></th>
												<th>Rate</th>
												<th># Of Pax</th>
												<th>Total Amount</th>
												<th>Commission</th>
												<th>Net Amount</th>
												<th>Instruction</span></th>
											</tr>
										</thead>
										<tbody>
											@foreach($sip as $sip)
												<tr>
													<td><span class="label-table black label-inherit upper-case">{{$sip->tour_date}}</span></td>
													<td><span class="label-table black label-inherit upper-case">{{$sip->particular}}</span></td>
													<td>
														<span class="label-table black label-inherit">{{number_format($sip->rate, 2, '.', ',')}}</span>
														@if($sip->foreign_rate)
															<?php $foreign_rate = $sip->rate + $sip->foreign_rate; ?>
															<br>
															<span class="label-table black label-inherit">{{number_format($foreign_rate, 2, '.', ',')}}</span>
														@endif
													</td>
													<td>
														@if($sip->pax)
															<span class="label-table black label-inherit">{{$sip->pax}} <span class="black label-pax">LCL</span></span>
														@endif
														@if($sip->foreign_pax)
															<br>
															<span class="label-table black label-inherit">{{$sip->foreign_pax}} <span class="black label-pax">FOR</span></span>
														@endif
													</td>
													<td><span class="label-table black label-inherit">{{number_format($sip->total, 2, '.', ',')}}</span></td>
													<td>
														@if($sip->commission)
															<span class="label-table black label-inherit">{{number_format($sip->commission, 2, '.', ',')}}</span>
														@endif
													</td>
													<?php $net_amount = $sip->total - $sip->commission; ?>
													<td><span class="label-table black label-inherit">{{number_format($net_amount, 2, '.', ',')}}</span></td>
													@foreach($sit as $sit)
														<td>{{$sit->description}} / {{$sit->highlights}}/{{$sit->inclusions}}/{{$sit->pick_up}}</td>
													@endforeach
												</tr>
												@php
													$total = $total + $net_amount;
												@endphp
											@endforeach
											@php
												$vat = ($total / 1.12) * .12;
											@endphp
											<tr>
												<td colspan="7"><span class="label-table label-inherit black upper-case"><strong>Total</strong></span></td>
												<td colspan="1"><span class="label-table label-inherit black">{{number_format($total, 2, '.', ',')}}</span></td>
											</tr>
											<tr>
												<td colspan="7"><span class="label-table label-inherit black upper-case"><strong>Vatable Sales</strong></span></td>
												<td colspan="1"><span class="label-table label-inherit black">{{number_format($total-$vat, 2, '.', ',')}}</span></td>
											</tr>
											<tr>
												<td colspan="7"><span class="label-table label-inherit black upper-case"><strong>12% Vat</strong></span></td>
												<td colspan="1"><span class="label-table label-inherit black">{{number_format($vat, 2, '.', ',')}}</span></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
				@endforeach
            </div>
        </div>
    </div>
</body>
</html>
						