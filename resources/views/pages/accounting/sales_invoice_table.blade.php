<x-app-layout>


	@include('pages.side_layout.accounting_side_layout')
        <!-- PAGE CONTAINER-->
	    <div class="page-container">
            <!-- HEADER DESKTOP-->
            <header class="header-desktop">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            <div class="title-3 text-uppercase">
                                <h3>Accounting Page</h3>
                            </div>
                            <div class="header-button">
                                <div class="account-wrap">
                                    <div class="account-item clearfix js-item-menu">
                                        @include('navigation-menu')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            		<!-- HEADER DESKTOP-->
			<x-slot name="header">
				<h2 class="font-semibold text-xl text-gray-800 leading-tight">
					{{ __('Sales Invoice Table | Accounting') }}
				</h2>
			</x-slot>
		    <!-- MAIN CONTENT-->
		    <div class="main-content">
			    <div class="section__content section__content--p30">
				    <div class="container-fluid">
					    <div class="row">
							<div class="col-md-12">
								<div class="au-card m-b-30">
									<div class="au-card-inner">
										<div class="header-title text-center">
											<h2>{{$soa_status}} SOA</h2>
											<input type="hidden" value="{{$soa_status}}" name="search_filter_status" class="search-filter-status">
										</div>
										<div class="col-xs-12 form-div">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<div class="form-group">
													<div class="btn-group btn-group-justified" role="group" aria-label="...">
														@if($soa_status === "Pending")
															<div class="btn-group" role="group">
																<a href="{{url('accounting/statement-of-account')}}" class="btn btn-dark-2-yellow btn-lg"><i class="fa fa-th-large"></i> Grid</a>
															</div>
															<a href="{{url('accounting/statement-of-account/table')}}" class="btn btn-dark-2-yellow btn-lg"><i class="fa fa-table"></i> Table</a>
														@else
															<div class="btn-group" role="group">
																<a href="{{url('accounting/statement-of-account/verified')}}" class="btn btn-dark-2-yellow btn-lg"><i class="fa fa-th-large"></i> Grid</a>
															</div>
															<a href="{{url('accounting/statement-of-account/table/verified')}}" class="btn btn-dark-2-yellow btn-lg"><i class="fa fa-table"></i> Table</a>
														@endif
													</div>
												</div>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-6">
												<div class="form-group">
													<div class="input-group">
														<span class="input-group-addon text-bold white bg-dark"><i class="fa fa-search"></i></span>
														<input type="text" class="form-control search-value" data-var="tours" placeholder="Search">
														<span class="input-group-btn">
															<div class="btn-group" role="group">
																<button type="button" class="btn btn-dark-2 btn-lg dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																	Search <span class="caret"></span>
																</button>
																<ul class="dropdown-menu bg-dark">
																	<li role="separator" class="divider"></li>
																	<li class="text-center"><a href="#" class="tour-code-black white filter-si-table-acc text-bold" data-filtval="si"><i class="fa fa-star orange"></i> SI #</a></li>
																	<li role="separator" class="divider"></li>
																	<li class="text-center"><a href="#" class="tour-code-black white filter-si-table-acc text-bold" data-filtval="date"><i class="fa fa-calendar blue"></i> Date</a></li>
																	<li role="separator" class="divider"></li>
																	<li class="text-center"><a href="#" class="tour-code-black white filter-si-table-acc text-bold" data-filtval="guest"><i class="fa fa-user green"></i> Guest</a></li>
																	<li role="separator" class="divider"></li>
																</ul>
															</div>
														</span>
													</div>
												</div>
											</div>

											<div class="col-xs-12 col-sm-12 col-md-3 text-right">

											<div class="btn-group btn-group-justified" role="group" aria-label="...">
													<div class="btn-group" role="group">
														<button type="button" class="btn btn-dark-2 btn-lg dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
															Rows <span class="caret"></span>
														</button>
														<ul class="dropdown-menu">
															<li role="separator" class="divider"></li>
															<li class="text-center"><a href="#" class="text-bold filter-si-table-row-acc si-options" data-act="filter" data-filtval="15">Default</a></li>
															<li role="separator" class="divider"></li>
															<li class="text-center"><a href="#" class="text-bold filter-si-table-row-acc si-options" data-act="filter" data-filtval="30">30</a></li>
															<li role="separator" class="divider"></li>
															<li class="text-center"><a href="#" class="text-bold filter-si-table-row-acc si-options" data-act="filter" data-filtval="45">45</a></li>
															<li role="separator" class="divider"></li>
															<li class="text-center"><a href="#" class="text-bold filter-si-table-row-acc si-options" data-act="filter" data-filtval="60">60</a></li>
															<li role="separator" class="divider"></li>
														</ul>
													</div>
												</div>

											</div>
											<input type="hidden" value="{{csrf_token()}}" name="deleteToken">
											<div class="col-xs-12 content-lower"></div>
											<div class="col-xs-12 text-center">
												<span class="label label-info text-bold size-15">Pending</span>
												<span class="label label-primary text-bold size-15">Verified</span>
												<span class="label bg-si-contact text-bold size-15">Complete</span>
											</div>
											<div class="col-xs-12 content-lower"></div>
											<div class="col-xs-12 searchResult">
												<table class="table table-striped table-hover table-condensed table-responsive">
													<thead>
														<tr>
															<th><span class="text-bold black">SI#</span></span></th>
															<th><span class="text-bold black">Guest</span></th>
															<th><span class="text-bold black">Particular</span></th>
															<th><span class="text-bold black">Agent</span></th>
															<th><span class="text-bold black">Reservation</span></th>
															<th><span class="text-bold black">Booking</span></th>
															<th><span class="text-bold black">Total</span></th>
															<th></th>
														</tr>
													</thead>
													<tbody>
														@php
															$count = 0;
														@endphp
														@foreach($si as $s)
															@php
																$_created = Carbon\Carbon::parse($s->created_at);
																$created = $_created->toDateString();
																$dispCreated = $_created->format('F d Y');
																$time = $_created->format('h:i:s A');
																$_pTime = Carbon\Carbon::parse($s->time);
																$pickup_time = $_pTime->format('h:i:s A');
																$count++
															@endphp
															<tr>
																<td><span class="label-table label-inherit">{{$s->sales_invoice}}</span></td>
																<td><span class="label-table label-inherit">{{$s->lead_guest}}</span></td>
																<td>
																	@foreach($sip as $particular)
																		@if($particular->si_id == $s->id)
																			<span class="label label-info">{{$particular->particular}}</span>
																		@endif
																	@endforeach
																</td>
																@foreach($sia as $agent)
																	@if($agent->si_id == $s->id)
																		<td><span class="label-table label-inherit">{{$agent->name}}</span></td>
																	@endif
																@endforeach
																<td><span class="label-table label-inherit">{{$s->reservation_officer}}</span></td>
																<td><span class="label-table label-inherit">{{$dispCreated}}</span></td>
																<td><span class="label bg-si-pickup-time">{{number_format($s->total, 2, '.', ',')}}</span></td>
																<td class="text-center vertical-middle row-pad accounting-verify narture-border" data-toggle="tooltip" data-placement="top" title="Update {{$s->sales_invoice}}">
																	<a href="{{url('accounting/check-soa').'/'.$s->id}}"><span class="white text-bold size-m"><i class="fa fa-pencil white"></i></span>
																</td>
															</tr>
														@endforeach
													</tbody>
												</table>
											</div>
											<div class="col-xs-12 text-center">
												{{	$si->links()	}}
											</div>
											<div class="page-wrapper">
											<!-- MENU SIDEBAR-->
											<aside class="menu-sidebar2">
												<div class="logo">
													<a href="#">
														<img src="images/icon/logo1.png" alt="logo" style="height: 75px;width: 95px;">
													</a>
												</div>
												<div class="menu-sidebar2__content js-scrollbar1">
													<div class="account2">
														<div class="image img-cir img-120">
															<img src="images/icon/avatar-big-01.jpg" alt="{{ Auth::user()->name }}" />
														</div>
														<h5 id="name2"></h5>
																<!-- Authentication -->
																<form method="POST" action="{{ route('logout') }}">
																	@csrf

																	<x-jet-dropdown-link href="{{ route('logout') }}"
																			onclick="event.preventDefault();
																					this.closest('form').submit();">
																		{{ __('Logout') }}
																	</x-jet-dropdown-link>
																</form>
													</div>
													<nav class="navbar-sidebar2">
														<ul class="list-unstyled navbar__list">
															<li><!-- Link with dropdown items -->
																<a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false"><i class="fas fa-clipboard-list"></i>SOA</a>
																<ul class="collapse list-unstyled navbar__list" id="homeSubmenu">
																	<li><a href="{{url('accounting/statement-of-account')}}"><i class="fa fa-btn fa-spinner fa-navbar"></i> Pending</a></li>
																	<li><a href="{{url('accounting/statement-of-account/verified')}}"><i class="fa fa-btn fa-check-circle fa-navbar"></i> Verified</a></li>
																	<li><a href="{{url('accounting/sales-report')}}"><i class="fa fa-btn fa-file-text fa-navbar"></i> Sales Report</a></li>
																</ul>
															</li>
															<li>
																<a href="#homeSubmenu1" data-toggle="collapse" aria-expanded="false"><i class="fas fa-clipboard-list"></i>Reports</a>
																<ul class="collapse list-unstyled navbar__list" id="homeSubmenu1">
																	<li><a href="#"><i class="fa fa-btn fa-signing fa-navbar"></i> Collection Report</a></li>
																	<li><a href=""><i class="fa fa-btn fa-file-text fa-navbar"></i> Income Statement Report</a></li>
																	<li><a href=""><i class="fa fa-btn fa-balance-scale fa-navbar"></i> Balance Sheet Report</a></li>
																	<li><a href="#"><i class="fa fa-btn fa-handshake-o fa-navbar"></i> Account Receivable</a></li>
																	<li><a href="#"><i class="fa fa-btn fa-hourglass-2 fa-navbar"></i> Aging of Account Receivable</a></li>
																	<li><a href="#"><i class="fa fa-btn fa-clock-o fa-navbar"></i> Account Payable Due</a></li>
																</ul>
															</li>
															<li><a href="{{url('accounting/tours')}}"><i class="fas fa-clipboard-list"></i>Tours</a></li>
															<li><a href="{{url('accounting/agents')}}"><i class="fas fa-clipboard-list"></i>Agents</a></li>
														</ul>
													</nav>
												</div>
											</aside>
										</div>
                                    </div>
                                </div>
                            </div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="copyright">
									<p>Copyright © 2021 CebuTripTours. All rights reserved. Template by <a href="#">Cebu Trip Tours</a>.</p>
								</div>
							</div>
						</div>
                    </div>
                </div>
			</div>
		</div>
    </div>
</x-app-layout>
