<x-app-layout>
                                '

    @include('pages.side_layout.accounting_side_layout')
        <!-- PAGE CONTAINER-->
	    <div class="page-container">
            <!-- HEADER DESKTOP-->
            <header class="header-desktop">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            <div class="title-3 text-uppercase">
                                <h3>Accounting Page</h3>
                            </div>
                            <div class="header-button">
                                <div class="account-wrap">
                                    <div class="account-item clearfix js-item-menu">
                                        @include('navigation-menu')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- HEADER DESKTOP-->
             <x-slot name="header">
                <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                    {{ __('Income Statement  | Accounting') }}
                </h2>
            </x-slot>
            <!-- MAIN CONTENT-->
		    <div class="main-content">
			    <div class="section__content section__content--p30">
				    <div class="container-fluid">
					    <div class="row">
						    <div class="col-md-12">
                                <div class="top-campaign">
                                        <h3 class="title-3 m-b-30">Income Statement Report</h3>
                                        <div class="table-responsive">
                                            <table class="table table-top-campaign">
                                                <thead>
                                                    <tr> 
                                                        <td>Agent Company</td>
                                                        <td>Tour</ttd>
                                                        <td>#SI</td>
                                                        <td>Sales</td>
                                                        <td>Status</td>
                                                                    
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($sip as $particular)
														@php
															$total = 0;
															$comm = 0;
															$total = $total + $particular->total;
															$comm = $comm + $particular->commission;
																	
															$net_amount = $total - $comm;
															$vat = ($net_amount / 1.12) * .12;
													    @endphp 
                                                        @foreach($sia as $sia) 
                                                            @foreach($data as $s)
                                                                <tr>
                                                                    <td>{{$sia->name}}</td>
                                                                    <td>{{$particular->particular}}</td>
                                                                    <td>{{$s->sales_invoice}}</td>
                                                                    <td>{{number_format($net_amount-$vat, 2, '.', ',')}}</td>
                                                                    <td><a href="#"></a></td>
                                                                </tr> 
                                                            @endforeach
                                                        @endforeach
                                                    @endforeach 
                                                </tbody>
                                            </table>
                                        </div>
                                </div><!--  END TOP CAMPAIGN-->
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="copyright">
                                    <p>Copyright © 2021 CebuTripTours. All rights reserved. Template by <a href="#">Cebu Trip Tours</a>.</p>
                                </div>
                            </div>
                        </div>
					</div>
				</div>
			</div>
		</div><!-- END PAGE CONTAINER-->
	</div>
</x-app-layout>
