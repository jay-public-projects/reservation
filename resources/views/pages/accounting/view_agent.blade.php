<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('View Agents | Accounting') }}
        </h2>
	</x-slot>

		<!-- MENU SIDEBAR-->
		@include('pages.side_layout.admin_side_layout')

			<!-- PAGE CONTAINER-->
			<div class="page-container">
                <!-- HEADER DESKTOP-->
                <header class="header-desktop">
                    <div class="section__content section__content--p30">
                        <div class="container-fluid">
                            <div class="header-wrap">
                                <div class="title-3 text-uppercase">
                                    <h3>Accounting Page</h3>
                                </div>
                                <div class="header-button">
                                    <div class="account-wrap">
                                        <div class="account-item clearfix js-item-menu">
                                            @include('navigation-menu')
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
                <!-- HEADER DESKTOP-->
				<!-- MAIN CONTENT-->
				<div class="main-content">
					<div class="section__content section__content--p30">
						<div class="container-fluid">
							<div class="row">
								<div class="col-lg-12">
									<div class="card">
										<div class="card-header">View Agent</div>
											<div class="card-body">
												<form action="{{ url('admin/edit/agent') }}" method="post">
													{{ csrf_field() }}
													<div class="row">
														<div class="col-lg-12">
															<div class="card">
																<div class="card-header">
																	<strong> View Agent Form</strong>
																</div>
																<div class="card-body card-block">
                                                                    @foreach($agent as $a)
																		<div class="row form-group">
																			<div class="col col-md-3">
																				<label for="text-input" class=" form-control-label"> Agent Name </label>
																			</div>
																			<div class="col-12 col-md-9">
																				<input type="text" id="name" name="name" value="{{$a->name}}" placeholder="Enter Agent Name" class="form-control" readonly>
																				<small class="form-text text-muted">Please enter the agent Name</small>
																			</div>
																		</div>
																		<div class="row form-group">
																			<div class="col col-md-3">
																				<label for="text-input" class=" form-control-label">Complete Adress </label>
																			</div>
																			<div class="col-12 col-md-9">
																				<input type="text" id="address" name="address" value="{{$a->address}}" placeholder="Enter Complete Address" class="form-control" readonly>
																				<small class="help-block form-text">Please enter the complete address</small>
																			</div>
																		</div>
																		<div class="row form-group">
																			<div class="col col-md-3">
																				<label for="select" class=" form-control-label">Business Type</label>
																			</div>
																			<div class="col-12 col-md-9">
																				<select name="nature" id="nature" class="form-control" readonly>
																					<option value="">Nature of Business</option>
                                                                                    <option  <?php if($a->nature === "Travel Agency"){echo "selected";} ?> value="Travel Agency">Travel Agency</option>
																					<option  <?php if($a->nature === "OTA"){echo "selected";} ?> value="OTA">OTA</option>
																					<option  <?php if($a->nature === "Hotel"){echo "selected";} ?> value="Hotel">Hotel</option>
																					<option  <?php if($a->nature === "Corporate"){echo "selected";} ?> value="Corporate">Corporate</option>
																				</select>
																			</div>
																		</div>
																		<div class="row form-group">
																			<div class="col col-md-3">
																				<label for="text-input" class=" form-control-label">TIN </label>
																			</div>
																			<div class="col-12 col-md-9">
																				<input type="text" id="tin" name="tin" min="1" value="{{$a->tin}}" placeholder="Enter TIN" class="form-control" readonly>
																				<small class="help-block form-text">Please enter the tour TIN</small>
																			</div>
																		</div>
																		<div class="row form-group">
																			<div class="col col-md-3">
																				<label for="number-input" class=" form-control-label">Contract Rate </label>
																			</div>
																			<div class="col-12 col-md-9">
																				<input type="number" id="contract_rate" name="contract_rate" min="0" value="{{$a->contract_rate}}" placeholder="Enter Contract Rate" class="form-control" readonly>
																				<small class="form-text text-muted">Please enter the contract rate</small>
																			</div>
																		</div>
																		<div class="row form-group">
																			<div class="col col-md-3">
																				<label for="text-input" class=" form-control-label">Payment Terms </label>
																			</div>
																			<div class="col-12 col-md-9">
																				<input type="number" id="payment_terms" name="payment_terms" value="{{$a->payment_terms}}" placeholder="Enter Payment Terms" class="form-control" readonly>
																				<small class="help-block form-text">Please enter the payment terms</small>
																			</div>
																		</div>
																	@endforeach
																	@foreach($contacts as $c)
																		<div class="row form-group">
																			<div class="col col-md-3">
																				<label for="text-input" class=" form-control-label">Contact Name </label>
																			</div>
																			<div class="col-12 col-md-9">
																				<input type="text" id="contact_name" name="contact_name" value="{{$c->contact_name}}" placeholder="Enter Contact Name" class="form-control" readonly>
																				<small class="help-block form-text">Please enter the tour contact name</small>
																			</div>
																		</div>
																		<div class="row form-group">
																			<div class="col col-md-3">
																				<label for="number-input" class=" form-control-label">Contact Number </label>
																			</div>
																			<div class="col-12 col-md-9">
																				<input type="text" id="contact_number" name="contact_number" min="0" value="{{$c->contact_number}}" placeholder="Enter Contact Number" class="form-control" readonly>
																				<small class="form-text text-muted">Please enter the contact number</small>
																			</div>
																		</div>
																		<div class="row form-group">
																			<div class="col col-md-3">
																				<label for="text-input" class=" form-control-label">Contact Designation </label>
																			</div>
																			<div class="col-12 col-md-9">
																				<input type="number" id="contact_designation" name="contact_designation" value="{{$c->contact_designation}}" placeholder="Enter Contact Designation" class="form-control" readonly>
																				<small class="help-block form-text">Please enter the contact designation</small>
																			</div>
																		</div>
																		<div class="row form-group">
																			<div class="col col-md-3">
																				<label for="email-input" class=" form-control-label">Contact Email </label>
																			</div>
																			<div class="col-12 col-md-9">
																				<input type="email" id="contact_email" name="contact_email" value="{{$c->contact_email}}" placeholder="Enter Contact Email" class="form-control" readonly>
																				<small class="help-block form-text">Please enter the contact email</small>
																			</div>
																		</div>
                                                                    @endforeach
																</div>
															</div>
														</div>
														<div class="col-lg-12">
															<button type="submit" class="btn btn-blue btn-lg btn-submit col-lg-12"><i class="fa fa-save"></i> Commit</button>
														</div>
													</div>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="copyright">
										<p>Copyright © 2021 CebuTripTours. All rights reserved. Template by <a href="#">Cebu Trip Tours</a>.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
</x-app-layout>
