<x-app-layout>

    <!-- MENU SIDEBAR-->
	@include('pages.side_layout.accounting_side_layout')
        <!-- PAGE CONTAINER-->
	    <div class="page-container">
		    		<!-- HEADER DESKTOP-->
					<header class="header-desktop">
						<div class="section__content section__content--p30">
							<div class="container-fluid">
								<div class="header-wrap">
                                    <div class="title-3 text-uppercase">
                                        <h3>Accounting Page</h3>
                                    </div>
									<div class="header-button">
										<div class="account-wrap">
											<div class="account-item clearfix js-item-menu">
												@include('navigation-menu')
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</header>
            		<!-- HEADER DESKTOP-->
             <x-slot name="header">
                <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                    {{ __('Sales Report | Accounting') }}
                </h2>
            </x-slot>
            <!-- MAIN CONTENT-->
		    <div class="main-content">
			    <div class="section__content section__content--p30">
				    <div class="container-fluid">
					    <div class="row">
							<div class="col-md-12">
								<div class="au-card m-b-30">
									<div class="au-card-inner">
										<div class="header-title text-center">
											<h2>Sales Report</h2>
										</div>
										<div class="col-xs-12 form-div">
											<div class="col-xs-12 col-sm-12 col-md-4">
												<div class="form-group">
													<div class="input-group">
														<span class="input-group-addon text-bold white bg-dark"><i class="fa fa-calendar-check-o"></i></span>
														<input type="text" class="form-control search-value sr-date sr-start" data-var="tours" placeholder="Start Date" value="{{$dispNow}}">
													</div>
												</div>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-4">
												<div class="form-group">
													<div class="input-group">
														<span class="input-group-addon text-bold white bg-dark"><i class="fa fa-calendar-times-o"></i></span>
														<input type="text" class="form-control search-value sr-date sr-end" data-var="tours" placeholder="End Date" value="{{$dispNow}}">
													</div>
												</div>
											</div>

											<div class="col-xs-12 col-sm-12 col-md-4 text-right">
												<button type="button" class="btn btn-dark-2 btn-lg btn-block sr-generate">
													<i class="fa fa-retweet white"></i> Generate Report
												</button>
											</div>

											<input type="hidden" value="{{csrf_token()}}" name="deleteToken">
											<div class="col-xs-12 content-lower"></div>
											<div class="col-xs-12 searchResult">
												<table class="table table-striped table-hover table-condensed table-responsive">
													<thead>
														<tr>
															<th><span class="text-bold black">SI#</span></span></th>
															<th><span class="text-bold black">Guest</span></th>
															<th><span class="text-bold black">Particular</span></th>
															<th><span class="text-bold black">Agent</span></th>
															<th><span class="text-bold black">Reservation</span></th>
															<th><span class="text-bold black">Booking</span></th>
															<th><span class="text-bold black">Total</span></th>
															<th></th>
														</tr>
													</thead>
													<tbody>
														@php
															$count = 0;
															$sr_total = 0;
														@endphp
														@foreach($si as $s)
															@php
																$_created = Carbon\Carbon::parse($s->created_at);
																$created = $_created->toDateString();
																$dispCreated = $_created->format('F d Y');
																$time = $_created->format('h:i:s A');
																$_pTime = Carbon\Carbon::parse($s->time);
																$pickup_time = $_pTime->format('h:i:s A');
																$count++;
																$comm = 0;
															@endphp
															<tr>
																<td><span class="label-table label-inherit">{{$s->sales_invoice}}</span></td>
																<td><span class="label-table label-inherit">{{$s->lead_guest}}</span></td>
																<td>
																	@foreach($sip as $particular)
																		@if($particular->si_id == $s->id)
																			<span class="label label-info">{{$particular->particular}}</span>
																			@php
																				$comm += $particular->commission;
																			@endphp
																		@endif
																	@endforeach
																</td>
																@foreach($sia as $agent)
																	@if($agent->si_id == $s->id)
																		<td><span class="label-table label-inherit">{{$agent->name}}</span></td>
																	@endif
																@endforeach
																@php
																	$net = $s->total - $comm;
																	$sr_total += $net;
																@endphp
																<td><span class="label-table label-inherit">{{$s->reservation_officer}}</span></td>
																<td><span class="label-table label-inherit">{{$dispCreated}}</span></td>
																<td><span class="label bg-si-pickup-time">{{number_format($net, 2, '.', ',')}}</span></td>
																<td class="text-center vertical-middle row-pad accounting-verify narture-border">
																	<a href="{{url('accounting/check-soa').'/'.$s->id}}"><span class="white text-bold size-m"><i class="fa fa-search white"></i></span>
																</td>
															</tr>
														@endforeach
													</tbody>
														@php
															$vat = ($sr_total / 1.12) * .12;
															$sr_vatable = $sr_total - $vat;
														@endphp
													<tfoot>
														<tr>
															<td class="sr-total-div"></td>
															<td class="text-center sr-total-div" colspan="2">
																<span class="label-table label-inherit">Total Net Sales</span><br>
																<h3>{{number_format($sr_total, 2, '.', ',')}}</h3>
															</td>
															<td class="text-center sr-total-div" colspan="2">
																<span class="label-table label-inherit">VAT</span><br>
																<h3>{{number_format($vat, 2, '.', ',')}}</h3>
															</td>
															<td class="text-center sr-total-div" colspan="3">
																<span class="label-table label-inherit">Total Vatable Sales</span><br>
																<h3>{{number_format($sr_vatable, 2, '.', ',')}}</h3>
															</td>
														</tr>
													</tfoot>
												</table>
												<a href="{{url('accounting/print-sales-report').'/'.$dispNow.'/'.$dispNow}}" class="btn btn-green btn-block"><i class="fa fa-window-restore"></i> Print / Download</a>
											</div>
                                    </div>
                                </div>
                            </div>
						</div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="copyright">
								<p>Copyright © 2021 CebuTripTours. All rights reserved. Template by <a href="#">Cebu Trip Tours</a>.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</div>
    </div>
</x-app-layout>
