<x-app-layout>
                                '

    @include('pages.side_layout.accounting_side_layout')
        <!-- PAGE CONTAINER-->
	    <div class="page-container">
            <!-- HEADER DESKTOP-->
            <header class="header-desktop">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            <div class="title-3 text-uppercase">
                                <h3>Accounting Page</h3>
                            </div>
                            <div class="header-button">
                                <div class="account-wrap">
                                    <div class="account-item clearfix js-item-menu">
                                        @include('navigation-menu')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- HEADER DESKTOP-->
             <x-slot name="header">
                <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                    {{ __('Aging of Accounts Receivable  | Accounting') }}
                </h2>
            </x-slot>
            <!-- MAIN CONTENT-->
		    <div class="main-content">
			    <div class="section__content section__content--p30">
				    <div class="container-fluid">
					    <div class="row">
						    <div class="col-md-12">
							    <div class="card">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <!-- TOP CAMPAIGN-->
                                            <div class="top-campaign">
                                                <h3 class="title-3 m-b-30">Aging of Accounts Receivable</h3>
                                                <div class="table-responsive">
                                                    <table class="table table-top-campaign">
                                                        <thead>
                                                            <tr>
                                                                <td> Lead Guest </td>
                                                                <td># SOA </td>
                                                                <td>Current</td>
                                                                <!--<td>30 Days</td>
                                                                <td>60 Days</td>
                                                                <td>90 Days</td>
                                                                <td>120 Days</td>-->
                                                            </tr>

                                                        </thead>
                                                        <tbody>
                                                             @foreach($si as $s)
                                                                <tr>
                                                                    <td>{{ $s->lead_guest }} </td>
                                                                    <td>{{ $s->sales_invoice }}</td>
                                                                    <td>{{ $s->overall}}</td>
                                                                    <!--<td>$46,399.22</td>
                                                                    <td>$35,364.90</td>
                                                                    <td>$20,366.96</td>
                                                                    <td>$10,366.96</td>-->
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div><!--  END TOP CAMPAIGN-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="copyright">
                                    <p>Copyright © 2021 CebuTripTours. All rights reserved. Template by <a href="#">Cebu Trip Tours</a>.</p>
                                </div>
                            </div>
                        </div>
					</div>
				</div>
			</div>
		</div><!-- END PAGE CONTAINER-->
	</div>
</x-app-layout>
