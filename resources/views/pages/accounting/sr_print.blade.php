<!DOCTYPE html>
<html>
<head>
    <title>Sales Report</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    
</head>
<body>
	<table class="table">
		<thead>
			<tr>
				@if($start_date === $end_date)
					<th colspan="7" class="text-center"><h4 class="upper-case black">Sales Report ({{$start_date}})</h4></th>
				@else
					<th colspan="7" class="text-center"><h4 class="upper-case black">Sales Report ({{$start_date}} - {{$end_date}})</h4></th>
				@endif
			</tr>
			<tr>
				<th><span class="text-bold black">SI#</span></span></th>
				<th><span class="text-bold black">Guest</span></th>
				<th><span class="text-bold black">Particular</span></th>
				<th><span class="text-bold black">Agent</span></th>
				<th><span class="text-bold black">Reservation</span></th>
				<th><span class="text-bold black">Booking</span></th>
				<th><span class="text-bold black">Total</span></th>
			</tr>
		</thead>
		<tbody>
			@php
				$count = 0;
				$sr_total = 0;
			@endphp
			@foreach($si as $s)
				@php
					$_created = Carbon\Carbon::parse($s->created_at);
					$created = $_created->toDateString();
					$dispCreated = $_created->format('F d Y');
					$time = $_created->format('h:i:s A');
					$_pTime = Carbon\Carbon::parse($s->time);
					$pickup_time = $_pTime->format('h:i:s A');
					$count++;
					$comm = 0;
					$break = 0;
				@endphp
				<tr>
					<td><span class="label-table label-inherit">{{$s->sales_invoice}}</span></td>
					<td><span class="label-table label-inherit">{{$s->lead_guest}}</span></td>
					<td">
						@foreach($sip as $particular)
							@if($particular->si_id == $s->id)
								@if($break != 0)
									<span>,</span>
								@endif
								<span class="label-table label-inherit">{{$particular->particular}}</span>
								@php
									$break++;
									$comm += $particular->commission;
								@endphp
							@endif
						@endforeach
					</td>
					@foreach($sia as $agent)
						@if($agent->si_id == $s->id)
							<td><span class="label-table label-inherit">{{$agent->name}}</span></td>
						@endif
					@endforeach
					@php
						$net = $s->total - $comm;
						$sr_total += $net;
					@endphp
					<td><span class="label-table label-inherit">{{$s->reservation_officer}}</span></td>
					<td><span class="label-table label-inherit">{{$dispCreated}}</span></td>
					<td><span class="label-table label-inherit">{{number_format($net, 2, '.', ',')}}</span></td>
				</tr>
			@endforeach
		</tbody>
		@php
			$vat = ($sr_total / 1.12) * .12;
			$sr_vatable = $sr_total - $vat;
		@endphp
		<tfoot>
			<tr>
				<td class="text-left sr-total-div" colspan="6">
					<span class="label-table label-inherit black f-11">Total Net Sales</span>
				</td>
				<td colspan="1">
					<span class="label-table label-inherit black f-11">{{number_format($sr_total, 2, '.', ',')}}</span>
				</td>
			</tr>
			<tr>
				<td class="text-left sr-total-div" colspan="6">
					<span class="label-table label-inherit black f-11">Total Vatable Sales</span>
				</td>
				<td colspan="1">
					<span class="label-table label-inherit black f-11">{{number_format($sr_vatable, 2, '.', ',')}}</span>
				</td>
			</tr>
			<tr>
				<td class="text-left sr-total-div" colspan="6">
					<span class="label-table label-inherit black f-11">12% VAT</span>
				</td>
				<td colspan="1">
					<span class="label-table label-inherit black f-11">{{number_format($vat, 2, '.', ',')}}</span>
				</td>
			</tr>
		</tfoot>
	</table>

    

</body>
</html>