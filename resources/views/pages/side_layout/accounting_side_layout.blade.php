<div class="page-wrapper">
    <!-- HEADER MOBILE-->
    <header class="header-mobile d-block d-lg-none">
        <div class="header-mobile__bar">
            <div class="container-fluid">
                <div class="header-mobile-inner">
                    <a class="logo" href="#">
                        <img src="{{ asset('../img/logo.png') }}" alt="Cebu City Tours" />
                    </a>
                </div>
            </div>
        </div>
        <nav class="navbar-mobile">
            <div class="container-fluid">
                <ul class="navbar-mobile__list list-unstyled">
                    <li class="has-sub">
                        <a class="js-arrow" href="#" data-toggle="collapse" data-target="#soa-collapse1"
                            aria-expanded="false" aria-controls="soa-collapse1">
                            <i class="fa fa-copy"></i>SOA
                        </a>
                        <ul class="collapse" id="soa-collapse1">
                            <li>
                                <a href="{{url('accounting/statement-of-account')}}">
                                    <i class="fa fa-caret-right" aria-hidden="true"></i>
                                    Pending
                                </a>
                            </li>
                            <li>
                                <a href="{{url('accounting/statement-of-account/verified')}}">
                                    <i class="fa fa-caret-right" aria-hidden="true"></i>
                                    Verified
                                </a>
                            </li>
                            <li>
                                <a href="{{url('accounting/sales-report')}}">
                                    <i class="fa fa-caret-right" aria-hidden="true"></i>
                                    Sales Report
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="has-sub">
                        <a class="js-arrow" href="#" data-toggle="collapse" data-target="#report-collapse1"
                            aria-expanded="false" aria-controls="report-collapse1">
                            <i class="fa fa-file-text"></i> Report
                        </a>
                        <ul class="collapse" id="report-collapse1">
                            <li>
                                <a href="{{ url('accounting/collectible-accounts') }}">
                                    <i class="fa fa-caret-right" aria-hidden="true"></i>
                                    Collection Report
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('accounting/income-statements') }}">
                                    <i class="fa fa-caret-right" aria-hidden="true"></i>
                                    Income Statement Report
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-btn fa-balance-scale fa-navbar"></i>
                                    Balance Sheet Report
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-btn fa-handshake-o fa-navbar"></i>
                                    Account Receivable
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('accounting/aging-of-accounts-receivable') }}">
                                    <i class="fa fa-btn fa-hourglass-2 fa-navbar"></i>
                                    Aging of Account Receivable
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-btn fa-clock-o fa-navbar"></i>
                                    Account Payable Due
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="{{url('accounting/agents')}}">
                            <i class="fa fa-user-circle"></i>
                            Agents
                        </a>
                    </li>
                    <li>
                        <a href="{{url('accounting/tours')}}">
                            <i class="fa fa-plane"></i>
                            Tours
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- END HEADER MOBILE-->

    <!-- MENU SIDEBAR-->
    <aside class="menu-sidebar d-none d-lg-block">
        <div class="logo">
            <a href="#">
                <img src="{{ asset('../img/logo.png') }}" alt="Cebu Trip Tours" />
            </a>
        </div>
        <div class="menu-sidebar__content js-scrollbar1">
            <nav class="navbar-sidebar">
                <ul class="list-unstyled navbar__list">
                    <li class="has-sub">
                        <a class="js-arrow" href="#" data-toggle="collapse" data-target="#soa-collapse"
                            aria-expanded="false" aria-controls="soa-collapse">
                            <i class="fa fa-copy"></i>
                            SOA
                        </a>
                        <ul class="collapse" id="soa-collapse">
                            <li>
                                <a href="{{url('accounting/statement-of-account')}}"
                                   class="{{ (request()->is('accounting/statement-of-account')) ? 'active' : '' }}">
                                    <i class="fa fa-caret-right" aria-hidden="true"></i>
                                    Pending
                                </a>
                            </li>
                            <li>
                                <a href="{{url('accounting/statement-of-account/verified')}}"
                                   class="{{ (request()->is('accounting/statement-of-account/verified')) ? 'active' : '' }}">
                                    <i class="fa fa-caret-right" aria-hidden="true"></i>
                                    Verified
                                </a>
                            </li>
                            <li>
                                <a href="{{url('accounting/sales-report')}}"
                                   class="{{ (request()->is('accounting/sales-report')) ? 'active' : '' }}">
                                    <i class="fa fa-caret-right" aria-hidden="true"></i>
                                    Sales Report
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="has-sub">
                        <a class="js-arrow" href="#" data-toggle="collapse" data-target="#report-collapse"
                            aria-expanded="false" aria-controls="report-collapse">
                            <i class="fa fa-file-text" aria-hidden="true"></i>
                            Report
                        </a>
                        <ul class="collapse" id="report-collapse">
                            <li>
                                <a href="{{ url('accounting/collectible-accounts') }}"
                                   class="{{ (request()->is('accounting/collectible-accounts')) ? 'active' : '' }}">
                                    <i class="fa fa-caret-right" aria-hidden="true"></i>
                                    Collection Report
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('accounting/income-statements') }}"
                                class="{{ (request()->is('accounting/income-statements')) ? 'active' : '' }}">
                                    <i class="fa fa-caret-right" aria-hidden="true"></i>
                                    Income Statement Report
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <i class="fa fa-caret-right" aria-hidden="true"></i>
                                    Balance Sheet Report
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-caret-right" aria-hidden="true"></i>
                                    Account Receivable
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('accounting/aging-of-accounts-receivable') }}"
                                   class="{{ (request()->is('accounting/aging-of-accounts-receivable')) ? 'active' : '' }}">
                                    <i class="fa fa-caret-right" aria-hidden="true"></i>
                                    Aging of Account Receivable
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-caret-right" aria-hidden="true"></i>
                                    Account Payable Due
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="{{url('accounting/agents')}}"
                           class="{{ (request()->is('accounting/agents')) ? 'active' : '' }}">
                            <i class="fa fa-user-circle"></i>
                            Agents
                        </a>
                    </li>
                    <li>
                        <a href="{{url('accounting/tours')}}"
                           class="{{ (request()->is('accounting/tours')) ? 'active' : '' }}">
                            <i class="fa fa-plane"></i>
                            Tours
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </aside>
    <!-- END MENU SIDEBAR-->






