<div class="page-wrapper">
    <!-- HEADER MOBILE-->
    <header class="header-mobile d-block d-lg-none">
        <div class="header-mobile__bar">
            <div class="container-fluid">
                <div class="header-mobile-inner">
                    <a class="logo" href="#">
                        <img src="{{ asset('../img/logo.png') }}" alt="Cebu City Tours" />
                    </a>
                </div>
            </div>
        </div>
        <nav class="navbar-mobile">
            <div class="container-fluid">
                <ul class="navbar-mobile__list list-unstyled">
                    <li class="a-navbar"><a class="white text-bold nav-hover" href="{{url('reservation/salesinvoice')}}"><i class="fa fa-btn fa-folder-open fa-navbar"></i> Sales Invoice</a></li>
                    <li class="a-navbar"><a class="white text-bold nav-hover" href="{{url('reservation/tours')}}"><i class="fa fa-btn fa-leaf fa-navbar"></i> Tours</a></li>
                    <li class="a-navbar"><a class="white text-bold nav-hover" href="{{url('reservation/agents')}}"><i class="fa fa-btn fa-user-secret fa-navbar"></i> Agents</a></li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- END HEADER MOBILE-->

    <!-- MENU SIDEBAR-->
    <aside class="menu-sidebar d-none d-lg-block">
        <div class="logo">
            <a href="#">
                <img src="{{ asset('../img/logo.png') }}" alt="Cebu Trip Tours" />
            </a>
        </div>
        <div class="menu-sidebar__content js-scrollbar1">
            <nav class="navbar-sidebar">
                <ul class="list-unstyled navbar__list">
                <li class="a-navbar">
                    <a href="{{url('reservation/salesinvoice')}}"
                       class="{{ (request()->is('reservation/salesinvoice')) ? 'active' : '' }}">
                        <i class="fa fa-btn fa-folder-open fa-navbar"></i>
                        Sales Invoice
                    </a>
                </li>
                    <li class="a-navbar">
                        <a href="{{url('reservation/tours')}}"
                           class="{{ (request()->is('reservation/tours')) ? 'active' : '' }}">
                            <i class="fa fa-btn fa-plane"></i>
                            Tours
                        </a>
                    </li>
                    <li class="a-navbar">
                        <a href="{{url('reservation/agents')}}"
                           class="{{ (request()->is('reservation/agents')) ? 'active' : '' }}">
                            <i class="fa fa-btn fa-user-circle"></i>
                            Agents
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </aside>
    <!-- END MENU SIDEBAR-->










