<div class="page-wrapper">
		<!-- HEADER MOBILE-->
		<header class="header-mobile d-block d-lg-none">
			<div class="header-mobile__bar">
				<div class="container-fluid">
					<div class="header-mobile-inner">
						<a class="logo" href="#">
                            <img src="{{ asset('../img/logo.png') }}" alt="Cebu City Tours" />
                        </a>
					</div>
				</div>
			</div>
			<nav class="navbar-mobile">
				<div class="container-fluid">
					<ul class="navbar-mobile__list list-unstyled">
						<li>
                            <a href="{{url('admin/users')}}">
								<i class="fas fa-table"></i>Users</a>
						</li>
						<li>
                            <a href="{{url('admin/tours')}}">
								<i class="far fa-check-square"></i>Tours</a>
						</li>
                        <li>
                            <a href="{{url('admin/agents')}}">
								<i class="fas fa-table"></i>Agents</a>
						</li>
					</ul>
				</div>
			</nav>
		</header>
		<!-- END HEADER MOBILE-->

		<!-- MENU SIDEBAR-->
		<aside class="menu-sidebar d-none d-lg-block">
			<div class="logo">
				<a href="#">
					<img src="{{ asset('../img/logo.png') }}" alt="Cebu Trip Tours" />
				</a>
			</div>
			<div class="menu-sidebar__content js-scrollbar1">
				<nav class="navbar-sidebar">
					<ul class="list-unstyled navbar__list">
                        <li>
                            <a href="{{url('admin/users')}}" class="{{ (request()->is('admin/users')) ? 'active' : '' || (request()->is('admin/user')) ? 'active' : '' || (request()->is('admin/edit/user/*')) ? 'active' : '' }}">
                                <i class="fa fa-users" aria-hidden="true"></i> Users
                            </a>
						</li>
						<li>
                            <a href="{{url('admin/tours')}}" class="{{ (request()->is('admin/tours')) ? 'active' : ''  || (request()->is('admin/tour')) ? 'active' : '' || (request()->is('admin/edit/tour/*')) ? 'active' : '' }}">
                                <i class="fa fa-plane" aria-hidden="true"></i> Tours
                            </a>
						</li>
                        <li>
                            <a href="{{url('admin/agents')}}" class="{{ (request()->is('admin/agents')) ? 'active' : ''  || (request()->is('admin/agent')) ? 'active' : '' || (request()->is('admin/edit/agent/*')) ? 'active' : '' }}">
                                <i class="fa fa-user-circle" aria-hidden="true"></i> Agents
                            </a>
						</li>
					</ul>
				</nav>
			</div>
		</aside>
		<!-- END MENU SIDEBAR-->









