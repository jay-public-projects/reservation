<x-app-layout>
    <!-- MENU SIDEBAR-->
@include('pages.side_layout.admin_side_layout')

<!-- PAGE CONTAINER-->
    <div class="page-container">
        <!-- HEADER DESKTOP-->
        <header class="header-desktop">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <div class="header-wrap">
                        <div class="title-3 text-uppercase">
                            <h3>Administrator Page</h3>
                        </div>
                        <div class="header-button">
                            <div class="account-wrap">
                                <div class="account-item clearfix js-item-menu">
                                    @include('navigation-menu')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- HEADER DESKTOP-->
        <x-slot name="header">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('Edit Agents | Admin') }}
            </h2>
        </x-slot>
        <!-- MAIN CONTENT-->
        <div class="main-content">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h3 class="float-left content-title">Edit Agent</h3>
                        </div>
                    </div>

                    <div class="card-box page">
                        <form action="{{ url("admin/edit/agent/{$agent->id}") }}" method="post">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="name">Agent Name <i class="fa fa-asterisk icn-required"
                                                                        aria-hidden="true"></i></label>
                                        <input type="text" id="name" name="name" value="{{$agent->name}}"
                                               class="form-control {{ $errors->has('name') ? 'is-invalid' : ''}}">
                                        <small class="form-text text-muted">Please enter agent name</small>
                                        @if($errors->has('name'))
                                            <span class="has-error">Required</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="name">Complete Address <i class="fa fa-asterisk icn-required"
                                                                              aria-hidden="true"></i></label>
                                        <input type="text" id="address" name="address" value="{{$agent->address}}"
                                               class="form-control {{ $errors->has('address') ? 'is-invalid' : ''}}">
                                        <small class="form-text text-muted">Please enter complete address</small>
                                        @if($errors->has('address'))
                                            <span class="has-error">Required</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="name">Business Type <i class="fa fa-asterisk icn-required"
                                                                           aria-hidden="true"></i></label>
                                        <select name="nature" id="nature"
                                                class="form-control {{ $errors->has('nature') ? 'is-invalid' : ''}}">
                                            <option value="">--Select--</option>
                                            <option value="Travel Agency" {{old('nature', $agent->nature) == 'Travel Agency' ? 'selected' : ''}}>Travel Agency</option>
                                            <option value="OTA" {{old('nature', $agent->nature) == 'OTA' ? 'selected' : ''}}>OTA</option>
                                            <option value="Hotel" {{old('nature', $agent->nature) == 'Hotel' ? 'selected' : ''}}>Hotel</option>
                                            <option value="Corporate" {{old('nature', $agent->nature) == 'Corporate' ? 'selected' : ''}}>Corporate</option>
                                        </select>
                                        <small class="form-text text-muted">Please choose nature of business</small>
                                        @if($errors->has('nature'))
                                            <span class="has-error">Required</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="name">TIN <i class="fa fa-asterisk icn-required"
                                                                 aria-hidden="true"></i></label>
                                        <input type="text" id="tin" name="tin" min="1" value="{{$agent->tin}}"
                                               class="form-control {{ $errors->has('tin') ? 'is-invalid' : ''}}">
                                        <small class="help-block form-text">Please enter agent TIN</small>
                                        @if($errors->has('tin'))
                                            <span class="has-error">Required</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="name">Contract Rate <i class="fa fa-asterisk icn-required"
                                                                           aria-hidden="true"></i></label>
                                        <input type="number" id="contract_rate" name="contract_rate" min="0"
                                               value="{{$agent->contract_rate}}"
                                               class="form-control {{ $errors->has('contract_rate') ? 'is-invalid' : ''}}">
                                        <small class="form-text text-muted">Please enter contract rate</small>
                                        @if($errors->has('contract_rate'))
                                            <span class="has-error">Required</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="name">Payment Terms <i class="fa fa-asterisk icn-required"
                                                                           aria-hidden="true"></i></label>
                                        <input type="number" id="payment_terms" name="payment_terms"
                                               value="{{$agent->payment_terms}}"
                                               class="form-control {{ $errors->has('payment_terms') ? 'is-invalid' : ''}}">
                                        <small class="form-text text-muted">Please enter payment terms</small>
                                        @if($errors->has('payment_terms'))
                                            <span class="has-error">Required</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="name">Contact Name <i class="fa fa-asterisk icn-required"
                                                                          aria-hidden="true"></i></label>
                                        <input type="text" id="contact_name" name="contact_name"
                                               value="{{$contact->name}}"
                                               class="form-control {{ $errors->has('contact_name') ? 'is-invalid' : ''}}">
                                        <small class="form-text text-muted">Please enter contact name</small>
                                        @if($errors->has('contact_name'))
                                            <span class="has-error">Required</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="name">Contact Number <i class="fa fa-asterisk icn-required"
                                                                            aria-hidden="true"></i></label>
                                        <input type="text" id="contact_number" name="contact_number"
                                               value="{{$contact->contact_number}}"
                                               class="form-control {{ $errors->has('contact_number') ? 'is-invalid' : ''}}">
                                        <small class="form-text text-muted">Please enter contact name</small>
                                        @if($errors->has('contact_number'))
                                            <span class="has-error">Required</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="name">Contact Designation <i class="fa fa-asterisk icn-required"
                                                                                 aria-hidden="true"></i></label>
                                        <input type="text" id="contact_designation" name="contact_designation"
                                               value="{{$contact->designation}}"
                                               class="form-control {{ $errors->has('contact_designation') ? 'is-invalid' : ''}}">
                                        <small class="form-text text-muted">Please enter the contact designation</small>
                                        @if($errors->has('contact_designation'))
                                            <span class="has-error">Required</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="name">Contact Email <i class="fa fa-asterisk icn-required"
                                                                           aria-hidden="true"></i></label>
                                        <input type="email" id="contact_email" name="contact_email"
                                               value="{{$contact->email}}"
                                               class="form-control {{ $errors->has('contact_email') ? 'is-invalid' : ''}}">
                                        <small class="form-text text-muted">Please enter contact email</small>
                                        @if($errors->has('contact_email'))
                                            <span class="has-error">Required</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="name">Note <i class="fa fa-asterisk icn-required"
                                                                  aria-hidden="true"></i></label>
                                        <textarea id="notes" name="notes" rows="4"
                                                  class="form-control {{ $errors->has('notes') ? 'is-invalid' : ''}}">{{$agent->notes}}</textarea>
                                        <small class="form-text text-muted">Please enter note</small>
                                        @if($errors->has('notes'))
                                            <span class="has-error">Required</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <button type="submit" class="btn btn-blue btn-lg btn-submit float-right">
                                        <i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Save Changes
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="copyright">
                                <p>Copyright © 2021 CebuTripTours. All rights reserved. Template by <a href="#">Cebu
                                        Trip Tours</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
