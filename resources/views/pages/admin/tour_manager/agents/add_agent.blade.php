<x-app-layout>
    <!-- MENU SIDEBAR-->
@include('pages.side_layout.admin_side_layout')

<!-- PAGE CONTAINER-->
    <div class="page-container">
        <!-- HEADER DESKTOP-->
        <header class="header-desktop">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <div class="header-wrap">
                        <div class="title-3 text-uppercase">
                            <h3>Administrator Page</h3>
                        </div>
                        <div class="header-button">
                            <div class="account-wrap">
                                <div class="account-item clearfix js-item-menu">
                                    @include('navigation-menu')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- HEADER DESKTOP-->
        <x-slot name="header">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('Add Agents | Admin') }}
            </h2>
        </x-slot>
        <!-- MAIN CONTENT-->
        <div class="main-content">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h3 class="float-left content-title">Add Agent</h3>
                        </div>
                    </div>

                    <div class="card-box page">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                Please correct the validation error(s).
                                {{--<ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>--}}
                            </div>
                        @endif
                        <form action="{{ url('admin/add/agent') }}" method="post">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="name">Agent Name <i class="fa fa-asterisk icn-required" aria-hidden="true"></i></label>
                                        <input type="text" id="name" name="name" value="{{old('name')}}" class="form-control {{ $errors->has('name') ? 'is-invalid' : ''}}">
                                        <small class="form-text text-muted">Please enter agent name</small>
                                        @if($errors->has('name'))
                                            <span class="has-error">Required</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="name">Complete Address <i class="fa fa-asterisk icn-required" aria-hidden="true"></i></label>
                                        <input type="text" id="address" name="address" value="{{old('address')}}" class="form-control {{ $errors->has('address') ? 'is-invalid' : ''}}" >
                                        <small class="form-text text-muted">Please enter complete address</small>
                                        @if($errors->has('address'))
                                            <span class="has-error">Required</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="name">Business Type <i class="fa fa-asterisk icn-required" aria-hidden="true"></i></label>
                                        <select name="nature" id="nature" class="form-control {{ $errors->has('nature') ? 'is-invalid' : ''}}" >
                                            <option value="">--Select--</option>
                                            <option value="Travel Agency" {{old('nature') == 'Travel Agency' ? 'selected' : ''}}>Travel Agency</option>
                                            <option value="OTA" {{old('nature') == 'OTA' ? 'selected' : ''}}>OTA</option>
                                            <option value="Hotel" {{old('nature') == 'Hotel' ? 'selected' : ''}}>Hotel</option>
                                            <option value="Corporate" {{old('nature') == 'Corporate' ? 'selected' : ''}}>Corporate</option>
                                        </select>
                                        <small class="form-text text-muted">Please choose nature of business</small>
                                        @if($errors->has('nature'))
                                            <span class="has-error">Required</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="name">TIN <i class="fa fa-asterisk icn-required" aria-hidden="true"></i></label>
                                        <input type="text" id="tin" name="tin" min="1" value="{{old('tin')}}" class="form-control {{ $errors->has('tin') ? 'is-invalid' : ''}}" >
                                        <small class="help-block form-text">Please enter agent TIN</small>
                                        @if($errors->has('tin'))
                                            <span class="has-error">Required</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="name">Contract Rate <i class="fa fa-asterisk icn-required" aria-hidden="true"></i></label>
                                        <input type="number" id="contract_rate" name="contract_rate" value="{{old('contract_rate')}}" min="0" class="form-control {{ $errors->has('contract_rate') ? 'is-invalid' : ''}}" >
                                        <small class="form-text text-muted">Please enter contract rate</small>
                                        @if($errors->has('contract_rate'))
                                            <span class="has-error">Required</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="name">Payment Terms <i class="fa fa-asterisk icn-required" aria-hidden="true"></i></label>
                                        <input type="number" id="payment_terms" name="payment_terms" value="{{old('payment_terms')}}" class="form-control {{ $errors->has('payment_terms') ? 'is-invalid' : ''}}">
                                        <small class="form-text text-muted">Please enter payment terms</small>
                                        @if($errors->has('payment_terms'))
                                            <span class="has-error">Required</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="name">Contact Name <i class="fa fa-asterisk icn-required" aria-hidden="true"></i></label>
                                        <input type="text" id="contact_name" name="contact_name" value="{{old('contact_name')}}" class="form-control {{ $errors->has('contact_name') ? 'is-invalid' : ''}}">
                                        <small class="form-text text-muted">Please enter contact name</small>
                                        @if($errors->has('contact_name'))
                                            <span class="has-error">Required</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="name">Contact Number <i class="fa fa-asterisk icn-required" aria-hidden="true"></i></label>
                                        <input type="text" id="contact_number" name="contact_number" value="{{old('contact_number')}}" class="form-control {{ $errors->has('contact_number') ? 'is-invalid' : ''}}">
                                        <small class="form-text text-muted">Please enter contact name</small>
                                        @if($errors->has('contact_number'))
                                            <span class="has-error">Required</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="name">Contact Designation <i class="fa fa-asterisk icn-required" aria-hidden="true"></i></label>
                                        <input type="text" id="contact_designation" name="contact_designation" value="{{old('contact_designation')}}" class="form-control {{ $errors->has('contact_designation') ? 'is-invalid' : ''}}">
                                        <small class="form-text text-muted">Please enter the contact designation</small>
                                        @if($errors->has('contact_designation'))
                                            <span class="has-error">Required</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="name">Contact Email <i class="fa fa-asterisk icn-required" aria-hidden="true"></i></label>
                                        <input type="email" id="contact_email" name="contact_email" value="{{old('contact_email')}}" class="form-control {{ $errors->has('contact_email') ? 'is-invalid' : ''}}">
                                        <small class="form-text text-muted">Please enter contact email</small>
                                        @if($errors->has('contact_email'))
                                            <span class="has-error">Required</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="name">Note <i class="fa fa-asterisk icn-required" aria-hidden="true"></i></label>
                                        <textarea id="notes" name="notes" rows="4" class="form-control {{ $errors->has('notes') ? 'is-invalid' : ''}}">{{old('notes')}}</textarea>
                                        <small class="form-text text-muted">Please enter note</small>
                                        @if($errors->has('notes'))
                                            <span class="has-error">Required</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <button type="submit" class="btn btn-blue btn-lg btn-submit float-right">
                                        <i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Save
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="copyright">
                                <p>Copyright © 2021 CebuTripTours. All rights reserved. Template by <a href="#">Cebu Trip Tours</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>

