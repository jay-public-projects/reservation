<x-app-layout>
    <!-- MENU SIDEBAR-->
@include('pages.side_layout.admin_side_layout')

<!-- PAGE CONTAINER-->
    <div class="page-container">
        <!-- HEADER DESKTOP-->
        <header class="header-desktop">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <div class="header-wrap">
                        <div class="title-3 text-uppercase">
                            <h3>Administrator Page</h3>
                        </div>
                        <div class="header-button">
                            <div class="account-wrap">
                                <div class="account-item clearfix js-item-menu">
                                    @include('navigation-menu')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- HEADER DESKTOP-->
        <x-slot name="header">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('Users | Admin') }}
            </h2>
        </x-slot>
        <!-- MAIN CONTENT-->
        <div class="main-content">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h3 class="float-left content-title">Agents</h3>
                            <div class="btn-lg float-right">
                                <a href="{{url('admin/agent')}}" class="btn btn-blue ">
                                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                    Add Agent
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-box page">
                        <div class="row">
                            <div class="col-lg-12">
                                @if(count($natures) > 1)
                                    <div class="float-right">
                                        <form class="form-header" action="" method="POST">
                                            <button type="button" class="btn btn-white dropdown-toggle mr-2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Nature <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li role="separator" class="divider"></li>
                                                @foreach($natures as $nature)
                                                    <li class="text-center"><a href="#" class="tour-code filter" data-act="filter" data-filtval="{{$nature}}">{{$nature}}</a></li>
                                                    <li role="separator" class="divider"></li>
                                                @endforeach
                                            </ul>
                                            <input class="au-input au-input--xl" type="text" name="search" data-var="level" placeholder="Search User" />
                                            <button class="au-btn--submit" type="submit" data-act="search">
                                                <i class="zmdi zmdi-search"></i>
                                            </button>
                                        </form>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">&nbsp;
                                <div class="table-responsive">
                                    <table class="table table-bordered datatable table-striped">
                                        <thead>
                                            <tr>
                                                <th width="25%">Nature of Business</th>
                                                <th>Name</th>
                                                <th width="100px"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($agents as $a)
                                            <tr class="tr-shadow">
                                                <td>{{$a->nature}}</td>
                                                <td>{{$a->name}}</td>
                                                <td align="center">
                                                    <a href="{{url('admin/edit/agent').'/'.$a->id}}"
                                                       class="text-bold br-edit">
                                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!-- END DATA TABLE -->
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="copyright">
                                <p>Copyright © 2021 CebuTripTours. All rights reserved. Template by <a href="#">Cebu Trip Tours</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
