<x-app-layout>
    <!-- MENU SIDEBAR-->
@include('pages.side_layout.admin_side_layout')

<!-- PAGE CONTAINER-->
    <div class="page-container">
        <!-- HEADER DESKTOP-->
        <header class="header-desktop">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <div class="header-wrap">
                        <div class="title-3 text-uppercase">
                            <h3>Administrator Page</h3>
                        </div>
                        <div class="header-button">
                            <div class="account-wrap">
                                <div class="account-item clearfix js-item-menu">
                                    @include('navigation-menu')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- HEADER DESKTOP-->
        <x-slot name="header">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('Add Tours | Admin') }}
            </h2>
        </x-slot>
        <!-- MAIN CONTENT-->
        <div class="main-content">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h3 class="float-left content-title">Update Tour</h3>
                            <a href="/admin/tours" class="pt-3 float-right"><i class="fa fa-arrow-circle-left"
                                                                               aria-hidden="true"></i> Back to list </a>
                        </div>
                    </div>

                    <div class="card-box page">

                        <form action="{{ url('admin/edit/tour/'.$tour->id ?? '') }}" method="post">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <h5>Form</h5>
                                            <br>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="name">Name <i class="fa fa-asterisk icn-required"
                                                                          aria-hidden="true"></i></label>
                                                <input type="text" id="name" name="name"
                                                       value="{{ old('name', $tour->name) }}"
                                                       class="form-control {{ $errors->has('name') ? 'is-invalid' : ''}}">
                                                <small class="form-text text-muted">Please enter tour name</small>
                                                @if($errors->has('name'))
                                                    <span class="has-error">{{ $errors->first('name') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="code">Code <i class="fa fa-asterisk icn-required"
                                                                          aria-hidden="true"></i></label>
                                                <input type="text" id="code" name="code"
                                                       value="{{old('code', $tour->code)}}"
                                                       class="form-control {{ $errors->has('code') ? 'is-invalid' : ''}}">
                                                <small class="help-block form-text">Please enter tour code</small>
                                                @if($errors->has('code'))
                                                    <span class="has-error">{{ $errors->first('code') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="type">Type <i class="fa fa-asterisk icn-required"
                                                                          aria-hidden="true"></i></label>
                                                <select name="type" id="type"
                                                        class="form-control {{ $errors->has('type') ? 'is-invalid' : ''}}">
                                                    <option value=""
                                                            @if(old('type', $tour->type)  == '') selected @endif>
                                                        --Select--
                                                    </option>
                                                    <option value="1"
                                                            @if(old('type', $tour->type)  == '1') selected @endif>Shared
                                                        Tour
                                                    </option>
                                                    <option value="2"
                                                            @if(old('type', $tour->type)  == '2') selected @endif>
                                                        Private Tour
                                                    </option>
                                                </select>
                                                <small class="help-block form-text">Please enter tour type</small>
                                                @if($errors->has('type'))
                                                    <span class="has-error">{{ $errors->first('type') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="duration">Duration (Hours) <i
                                                        class="fa fa-asterisk icn-required"
                                                        aria-hidden="true"></i></label>
                                                <input type="number" id="duration" name="duration" min="1"
                                                       value="{{old('duration', $tour->duration)}}"
                                                       class="form-control {{ $errors->has('duration') ? 'is-invalid' : ''}}">
                                                <small class="help-block form-text">Please enter tour duration</small>
                                                @if($errors->has('duration'))
                                                    <span class="has-error">{{ $errors->first('duration') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="lead_time">Lead Time (Hours) <i
                                                        class="fa fa-asterisk icn-required"
                                                        aria-hidden="true"></i></label>
                                                <input type="number" id="lead_time" name="lead_time" min="0"
                                                       value="{{old('lead_time', $tour->lead_time)}}"
                                                       class="form-control {{ $errors->has('lead_time') ? 'is-invalid' : ''}}">
                                                <small class="form-text text-muted">Please enter lead time</small>
                                                @if($errors->has('lead_time'))
                                                    <span class="has-error">{{ $errors->first('lead_time') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="description">Description <i
                                                        class="fa fa-asterisk icn-required"
                                                        aria-hidden="true"></i></label>
                                                <textarea name="description" id="description" rows="4"
                                                          class="form-control {{ $errors->has('description') ? 'is-invalid' : ''}}">{{old('description', $tour->description)}}</textarea>
                                                <small class="help-block form-text">Please enter description</small>
                                                @if($errors->has('description'))
                                                    <span class="has-error">{{ $errors->first('description') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Pickup Notes <i
                                                        class="fa fa-asterisk icn-required"
                                                        aria-hidden="true"></i></label>
                                                <textarea name="pickup" id="pickup" rows="4"
                                                          class="form-control {{ $errors->has('pickup') ? 'is-invalid' : ''}}">{{old('pickup', $tour->pick_up)}}</textarea>
                                                <small class="help-block form-text">Please enter the pickup
                                                    notes.</small>
                                                @if($errors->has('pickup'))
                                                    <span class="has-error">{{ $errors->first('pickup') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="highlights">Highlights <i
                                                        class="fa fa-asterisk icn-required"
                                                        aria-hidden="true"></i></label>
                                                <textarea name="highlights" id="highlights" rows="4"
                                                          class="form-control {{ $errors->has('highlights') ? 'is-invalid' : ''}}">{{old('highlights', $tour->highlights)}}</textarea>
                                                <small class="help-block form-text">Please enter highlights</small>
                                                @if($errors->has('highlights'))
                                                    <span class="has-error">{{ $errors->first('highlights') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="inclusions">Inclusions <i
                                                        class="fa fa-asterisk icn-required"
                                                        aria-hidden="true"></i></label>
                                                <textarea name="inclusions" id="inclusions" rows="4"
                                                          class="form-control {{ $errors->has('inclusions') ? 'is-invalid' : ''}}">{{old('inclusions', $tour->inclusions)}}</textarea>
                                                <small class="help-block form-text">Please enter inclusions</small>
                                                @if($errors->has('inclusions'))
                                                    <span class="has-error">{{ $errors->first('inclusions') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="min_pax">Minimum pax <i class="fa fa-asterisk icn-required"
                                                                                    aria-hidden="true"></i></label>
                                                <input type="number" id="min_pax" name="min_pax" min="1" max="12"
                                                       value="{{old('min_pax', $tour->min_pax)}}"
                                                       class="form-control {{ $errors->has('min_pax') ? 'is-invalid' : ''}}"
                                                       onchange="PaxMin()">
                                                <small class="help-block form-text">Please enter minimum pax</small>
                                                @if($errors->has('min_pax'))
                                                    <span class="has-error">{{ $errors->first('min_pax') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <h5>With Guide</h5>
                                            <br>
                                        </div>
                                        @for ($i = 1; $i <= 12; $i++)
                                            @php
                                                $tour_pax = isset($tour_paxes, $i) && isset($tour_paxes[$i - 1])
                                                    ? $tour_paxes[$i - 1]
                                                    : new \App\Models\Tour();
                                            @endphp
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <div class="input-group-addon pax">{{$i}} PAX</div>
                                                    <input type="number" id="w_amount{{$i}}" name="w_amount[{{$i}}]"
                                                           min="100" placeholder="Price"
                                                           value="{{old("w_amount.".$i, $tour_pax->with_guide)}}"
                                                           class="form-control {{ $errors->has('w_amount.'.$i) ? 'is-invalid' : ''}}"
                                                           require>
                                                    <input type="hidden" name="w_pax[{{$i}}]" value="{{$i}}">
                                                    <input type="hidden" name="pax_id[{{$i}}]" value="0">
                                                    <small class="help-block form-text">&nbsp;</small>
                                                    @if($errors->has('w_amount'.$i))
                                                        <span
                                                            class="has-error">{{ $errors->first("w_amount'.$i'") }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        @endfor
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <h5>Without Guide</h5>
                                            <br>
                                        </div>
                                        @for ($i = 1; $i <= 12; $i++)
                                            @php
                                                $tour_pax = isset($tour_paxes, $i) && isset($tour_paxes[$i - 1])
                                                    ? $tour_paxes[$i - 1]
                                                    : new \App\Models\Tour();
                                            @endphp
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <div class="input-group-addon pax">{{$i}} PAX</div>
                                                    <input type="number" id="wo_amount{{$i}}" name="wo_amount[{{$i}}]"
                                                           min="100" placeholder="Price"
                                                           value="{{old("wo_amount.".$i, $tour_pax->without_guide)}}"
                                                           class="form-control {{ $errors->has('wo_amount.'.$i) ? 'is-invalid' : ''}}"
                                                           require>
                                                    <input type="hidden" name="wo_pax[{{$i}}]" value="{{$i}}">
                                                    <input type="hidden" name="pax_id[{{$i}}]" value="0">
                                                    <small class="help-block form-text">&nbsp;</small>
                                                    @if($errors->has('wo_amount'.$i))
                                                        <span
                                                            class="has-error">{{ $errors->first("wo_amount'.$i'") }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        @endfor
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <hr>
                                            <br>
                                            <h5>Select Peak Date</h5>
                                            <br>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Date From</label>
                                                <input type="date" name="peak_from"
                                                       value="{{old("peak_from", $tour_peak->from->format('Y-m-d'))}}"
                                                       class="form-control {{ $errors->has('peak_from') ? 'is-invalid' : ''}}"
                                                       require>
                                                <small class="form-text text-muted">Please enter date from</small>
                                                @if($errors->has('peak_from'))
                                                    <span class="has-error">{{ $errors->first('peak_from') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Date To</label>
                                                <input type="date" name="peak_to" placeholder="Date To"
                                                       value="{{old("peak_to", $tour_peak->to->format('Y-m-d'))}}"
                                                       class="form-control {{ $errors->has('peak_to') ? 'is-invalid' : ''}}"
                                                       require>
                                                <small class="form-text text-muted">Please enter date to</small>
                                                @if($errors->has('peak_to'))
                                                    <span class="has-error">{{ $errors->first('peak_to') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Amount Type</label>
                                                <select id="peak_type" name="peak_type"
                                                        class="form-control {{ $errors->has('peak_type') ? 'is-invalid' : ''}}"
                                                        require>
                                                    <option class="option-style"
                                                            value="amount" {{ $tour_peak->amount_type == 'amount' ? 'selected' : '' }}>
                                                        Amount
                                                    </option>
                                                    <option class="option-style"
                                                            value="percent" {{ $tour_peak->amount_type == 'percent' ? 'selected' : '' }}>
                                                        Percentage
                                                    </option>
                                                </select>
                                                <small class="form-text text-muted">Please enter amount type</small>
                                                @if($errors->has('peak_type'))
                                                    <span class="has-error">{{ $errors->first('peak_type') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Amount</label>
                                                <input type="number" name="peak_amount"
                                                       value="{{old("peak_amount", $tour_peak->amount)}}"
                                                       class="form-control {{ $errors->has('peak_amount') ? 'is-invalid' : ''}}"
                                                       require>
                                                <small class="form-text text-muted">Please enter amount</small>
                                                @if($errors->has('peak_amount'))
                                                    <span class="has-error">{{ $errors->first('peak_amount') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <input type="hidden" name="peak_id" value="">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Foreign Rate Additional</label>
                                                <input type="number"
                                                       value="{{old("foreign_rate", $tour->foreign_rate)}}"
                                                       class="form-control {{ $errors->has('foreign_rate') ? 'is-invalid' : ''}}"
                                                       min="0" name="foreign_rate" placeholder="Price" require>
                                                <small class="form-text text-muted">Please enter roreign rate
                                                    additional</small>
                                                @if($errors->has('foreign_rate'))
                                                    <span class="has-error">Required</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <button type="submit" class="btn btn-blue btn-lg btn-submit float-right">
                                        <i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Save Changes
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="copyright">
                                <p>Copyright © 2021 CebuTripTours. All rights reserved. Template by <a href="#">Cebu
                                        Trip Tours</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
