<x-app-layout>
    <!-- MENU SIDEBAR-->
    @include('pages.side_layout.admin_side_layout')

    <!-- PAGE CONTAINER-->
    <div class="page-container">
        <!-- HEADER DESKTOP-->
        <header class="header-desktop">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <div class="header-wrap">
                        <div class="title-3 text-uppercase">
                            <h3>Administrator Page</h3>
                        </div>
                        <div class="header-button">
                            <div class="account-wrap">
                                <div class="account-item clearfix js-item-menu">
                                    @include('navigation-menu')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- HEADER DESKTOP-->
        <x-slot name="header">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('Tours | Admin') }}
            </h2>
        </x-slot>
        <!-- MAIN CONTENT-->
        <div class="main-content">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h3 class="float-left content-title">Tours</h3>
                            <div class="btn-lg float-right">
                                <a href="{{url('admin/tour')}}" class="btn btn-blue ">
                                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                    Add Tours
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-box page">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive mt-4">
                                    <table class="table table-bordered datatable table-striped">
                                        <thead>
                                            <tr>
                                                <th width="15%">Name</th>
                                                <th>Type</th>
                                                <th>Code</th>
                                                <th>Duration</th>
                                                <th>Min Pax</th>
                                                <th>Pick Up</th>
                                                <th>Foreign Rate</th>
                                                <th>Status</th>
                                                <th width="100px"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($tours as $tour)
                                            <tr class="tr-shadow">
                                                <td>{{$tour->name}}</td>
                                                <td>
                                                    @if($tour->type == '1')
                                                        Shared Tour
                                                    @endif
                                                    @if($tour->type == '2')
                                                        Private Tour
                                                    @endif
                                                </td>
                                                <td>{{$tour->code}} </td>
                                                <td>{{$tour->duration}} Hour(s)</td>
                                                <td>{{$tour->min_pax}} Pax</td>
                                                <td>{{$tour->pick_up}} </td>
                                                <td>{{$tour->foreign_rate}} </td>
                                                <td>
                                                    @if($tour->status == '0')
                                                        Inactive
                                                    @endif
                                                    @if($tour->status == '1')
                                                        Active
                                                    @endif</td>
                                                <td align="center">
                                                    <a href="{{url('admin/edit/tour').'/'.$tour->id}}"
                                                       class="text-bold br-edit">
                                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!-- END DATA TABLE -->
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="copyright">
                                <p>Copyright © 2021 CebuTripTours. All rights reserved. Template by <a href="#">Cebu Trip Tours</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- modal medium -->
    <div class="modal fade" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="viewModal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="peakdate">Tour Name</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                </div>
            </div>
        </div>
    </div>
    <!-- end modal medium -->
</x-app-layout>

