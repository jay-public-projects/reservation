<x-app-layout>
    <!-- MENU SIDEBAR-->
    @include('pages.side_layout.admin_side_layout')

    <!-- PAGE CONTAINER-->
    <div class="page-container">
        <!-- HEADER DESKTOP-->
        <header class="header-desktop">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <div class="header-wrap">
                        <div class="title-3 text-uppercase">
                            <h3>Administrator Page</h3>
                        </div>
                        <div class="header-button">
                            <div class="account-wrap">
                                <div class="account-item clearfix js-item-menu">
                                    @include('navigation-menu')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- HEADER DESKTOP-->
        <x-slot name="header">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('Edit Users | Admin') }}
            </h2>
        </x-slot>
        <!-- MAIN CONTENT-->
        <div class="main-content">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h3 class="float-left content-title">Edit User</h3>
                            <a href="/admin/users" class="pt-3 float-right"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Back to list </a>
                        </div>
                    </div>

                    <div class="card-box page">
                        @foreach($user as $user)
                        <form action="{{ url('admin/edit/user/'.$user->id ?? '') }}" method="post">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="name">Name <i class="fa fa-asterisk icn-required" aria-hidden="true"></i></label>
                                        <input type="text" id="name" name="name" value="{{ old('name', $user->name) }}" class="form-control form-control-lg {{ $errors->has('name') ? 'is-invalid' : ''}}" require>
                                        <small class="form-text text-muted">Please enter your name</small>
                                        @if($errors->has('name'))
                                            <span class="has-error">{{ $errors->first('name') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="username">Username <i class="fa fa-asterisk icn-required" aria-hidden="true"></i></label>
                                        <input type="text" id="username" name="username" value="{{ old('username', $user->username) }}" class="form-control form-control-lg {{ $errors->has('username') ? 'is-invalid' : ''}}" require>
                                        <small class="form-text text-muted">Please enter your username</small>
                                        @if($errors->has('username'))
                                            <span class="has-error">{{ $errors->first('username') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="level">Level <i class="fa fa-asterisk icn-required" aria-hidden="true"></i></label>
                                        <select name="level" id="level" class="form-control {{ $errors->has('level') ? 'is-invalid' : ''}}">
                                            <option value="" @if(old('level', $user->level)  == '') selected @endif>--Select--</option>
                                            <option value="0" @if(old('level', $user->level) == '0') selected @endif>Admin User</option>
                                            <option value="1" @if(old('level', $user->level) == '1') selected @endif>Reservation User</option>
                                            <option value="2" @if(old('level', $user->level) == '2') selected @endif>Accounting User</option>
                                        </select>
                                        <small class="form-text text-muted">Please enter user level</small>
                                        @if($errors->has('level'))
                                            <span class="has-error">{{ $errors->first('level') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="level">Status <i class="fa fa-asterisk icn-required" aria-hidden="true"></i></label>
                                        <select name="status" id="status" class="form-control {{ $errors->has('status') ? 'is-invalid' : ''}}">
                                            <option value="" @if(old('status', $user->status)  == '') selected @endif>--Select--</option>
                                            <option value="0" @if(old('status', $user->status) == '0') selected @endif>Inactive</option>
                                            <option value="1" @if(old('status', $user->status) == '1') selected @endif>Active</option>
                                        </select>
                                        <small class="form-text text-muted">Please enter user status</small>
                                        @if($errors->has('status'))
                                            <span class="has-error">{{ $errors->first('status') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <button type="submit" class="btn btn-blue btn-lg btn-submit-user float-right">
                                    <i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Save Changes
                                </button>
                            </div>
                        </form>
                        @endforeach
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="copyright">
                                <p>Copyright © 2021 CebuTripTours. All rights reserved. Template by <a href="#">Cebu Trip Tours</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
