<x-app-layout>
    <!-- MENU SIDEBAR-->
    @include('pages.side_layout.admin_side_layout')

    <!-- PAGE CONTAINER-->
    <div class="page-container">
        <!-- HEADER DESKTOP-->
        <header class="header-desktop">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <div class="header-wrap">
                        <div class="title-3 text-uppercase">
                            <h3>Administrator Page</h3>
                        </div>
                        <div class="header-button">
                            <div class="account-wrap">
                                <div class="account-item clearfix js-item-menu">
                                    @include('navigation-menu')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- HEADER DESKTOP-->
        <x-slot name="header">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('Users | Admin') }}
            </h2>
        </x-slot>
        <!-- MAIN CONTENT-->
        <div class="main-content">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h3 class="float-left content-title">Users</h3>
                            <div class="btn-lg float-right">
                                <a href="{{url('admin/user')}}" class="btn btn-blue ">
                                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                    Add User
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-box page">
                        <div class="row">
                            <div class="col-md-12">&nbsp;
                                <div class="table-responsive">
                                    <table class="table table-bordered datatable table-striped">
                                        <thead>
                                        <tr>
                                            <th>Username</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Status</th>
                                            <th>Level</th>
                                            <th width="100px"></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($users as $user)
                                            <tr class="tr-shadow">
                                                <td>{{$user->username}}</td>
                                                <td class="text-capitalize">{{$user->name}}</td>
                                                <td>{{$user->email}}</td>
                                                <td class="text-capitalize">
                                                    {{$user->statusName}}
                                                </td>
                                                <td class="text-capitalize">
                                                    {{$user->levelName}}
                                                </td>
                                                <td align="center">
                                                    <a href="{{url('admin/edit/user').'/'.$user->id}}"
                                                       class="text-bold br-edit">
                                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!-- END DATA TABLE -->
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="copyright">
                                <p>Copyright © 2021 CebuTripTours. All rights reserved. Template by <a href="#">Cebu Trip Tours</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
