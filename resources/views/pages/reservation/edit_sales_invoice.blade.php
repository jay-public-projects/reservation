<x-app-layout>

		<!-- MENU SIDEBAR-->
		@include('pages.side_layout.reservation_side_layout')
							<x-slot name="header">
								<h2 class="font-semibold text-xl text-gray-800 leading-tight">
									{{ __('Add Sales Invoice | Cebu Trip Tours') }}
								</h2>
							</x-slot>
			<!-- PAGE CONTAINER-->
			<div class="page-container">
					<!-- HEADER DESKTOP-->
					<header class="header-desktop">
						<div class="section__content section__content--p30">
							<div class="container-fluid">
								<div class="header-wrap">
                                    <div class="title-3 text-uppercase">
                                        <h3>Reservation Page</h3>
                                    </div>
									<div class="header-button">
										<div class="account-wrap">
											<div class="account-item clearfix js-item-menu">
												@include('navigation-menu')
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</header>
            		<!-- HEADER DESKTOP-->

				<form action="{{ url('reservation/add/salesinvoice')}}" method="POST"  class="form-horizontal">
					{{ csrf_field() }}
					<!-- MAIN CONTENT-->
					<div class="main-content">
						<div class="section__content section__content--p30">
							<div class="container-fluid">
								<div class="row">
									<div class="col-lg-12">
										<div class="card">
											<div class="card-header">Edit Sales Invoice</div>
											<div class="card-body">
												<div class="row">
													<div class="col-lg-7">
															<div class="card">
																<div class="card-header">
																	<strong> SI Form</strong>
																</div>
																<div class="card-body card-block">
																	@foreach($si as $s)
																		<div class="row form-group">
																			<div class="col col-md-3">
																				<label for="text-input" class=" form-control-label">Lead Guest </label>
																			</div>
																			<div class="col-12 col-md-9">
																				<input type="text" id="lead_guest" name="lead_guest" placeholder="Enter Lead Guest" value="{{$s->lead_guest}}" class="form-control {{ $errors->has('lead_guest') ? 'is-invalid' : ''}}"require>
																				<small class="form-text text-muted">Please enter your lead guest</small>
																				@if($errors->has('lead_guest'))
																					<span class="has-error">{{ $errors->first('lead_guest') }}</span>
																				@endif
																			</div>
																		</div>
																		<div class="row form-group">
																			<div class="col col-md-3">
																				<label for="text-input" class=" form-control-label">Booking Reference Number </label>
																			</div>
																			<div class="col-12 col-md-9">
																				<input type="text" id="booking_ref" name="booking_ref" placeholder="Enter Booking Reference" value="{{$s->booking_reference}}" class="form-control {{ $errors->has('booking_ref') ? 'is-invalid' : ''}}" require>
																				<small class="help-block form-text">Please enter your booking reference</small>
																				@if($errors->has('booking_ref'))
																					<span class="has-error">{{ $errors->first('booking_ref') }}</span>
																				@endif
																			</div>
																		</div>
																		<div class="row form-group">
																			<div class="col col-md-3">
																				<label for="email-input" class=" form-control-label">Email </label>
																			</div>
																			<div class="col-12 col-md-9">
																				<input type="email" id="email" name="email" placeholder="Email" value="{{$s->email}}" class="form-control {{ $errors->has('email') ? 'is-invalid' : ''}}" require>
																				<small class="help-block form-text">Please enter a email</small>
																				@if($errors->has('email'))
																					<span class="has-error">{{ $errors->first('email') }}</span>
																				@endif
																			</div>
																		</div>
																		<div class="row form-group">
																			<div class="col col-md-3">
																				<label for="text-input" class=" form-control-label">Contact Number </label>
																			</div>
																			<div class="col-12 col-md-9">
																				<input type="text" id="contact" name="contact" placeholder="Enter Contact Number" value="{{$s->contact}}" class="form-control {{ $errors->has('contact') ? 'is-invalid' : ''}}" require>
																				<small class="form-text text-muted">Please enter your contact number</small>
																				@if($errors->has('contact'))
																					<span class="has-error">{{ $errors->first('contact') }}</span>
																				@endif
																			</div>
																		</div>
																		<div class="row form-group">
																			<div class="col col-md-3">
																				<label for="text-input" class=" form-control-label">Pick Up </label>
																			</div>
																			<div class="col-12 col-md-9">
																				<input type="text" id="pick_up" name="pick_up" placeholder="Enter Pick Up" value="{{$s->pickup}}" class="form-control {{ $errors->has('pick_up') ? 'is-invalid' : ''}}" require>
																				<small class="help-block form-text">Please enter your pick up</small>
																				@if($errors->has('pick_up'))
																					<span class="has-error">{{ $errors->first('pick_up') }}</span>
																				@endif
																			</div>
																		</div>
																		<div class="row form-group">
																			<div class="col col-md-3">
																				<label for="time-input" class=" form-control-label">Time </label>
																			</div>
																			<div class="col-12 col-md-9">
																				<input type="time" id="time" name="time" placeholder="Enter Time" value="{{$s->time}}" class="form-control {{ $errors->has('time') ? 'is-invalid' : ''}}" require>
																				<small class="help-block form-text">Please enter a time</small>
																				@if($errors->has('time'))
																					<span class="has-error">{{ $errors->first('time') }}</span>
																				@endif
																			</div>
																		</div>
																		<div class="row form-group">
																				<div class="col col-md-3">
																					<label for="textarea-input" class=" form-control-label">Note </label>
																				</div>
																				<div class="col-12 col-md-9">
																					<textarea name="note" id="note" rows="9" placeholder="Content..." value="{{$s->note}}" class="form-control {{ $errors->has('note') ? 'is-invalid' : ''}}" require></textarea>
																					@if($errors->has('note'))
																						<span class="has-error">{{ $errors->first('note') }}</span>
																					@endif
																				</div>
																		</div>
																		<div class="col-xs-12 col-sm-12 col-md-6">
																			<div class="col-xs-12 text-center overall-total-div">
																				<span class="overall-total size-25 text-bold"></span>
																				<input type="hidden" class="overall-total-value" name="overall" value="0.0">
																			</div>
																		</div>
																	@endforeach
																</div>
															</div>
													</div>
													<div class="col-lg-5">
															<div class="card">
																<div class="col-ms-3">
																		<div class="card">
																			<div class="card-header">
																				<strong>Agent</strong>
																			</div>
																			<div class="card-body card-block">
																				<div class="row">
																					<div class="col-xs-4 col-sm-4 col-md-4 text-center">
																						<div class="si-agent">
																							<i class="fa fa-globe size-80 dark-blue agent-add-others" data-val="Web" data-agent-id="0" data-toggle="tooltip" data-placement="top" title="Websites"></i><br>
																							<label class="si-agent-label si-agent-add">Web</label>
																						</div>
																					</div>
																					<div class="col-xs-4 col-sm-4 col-md-4 text-center">
																						<div class="si-agent">
																							<i class="fa fa-universal-access size-80 blue-2 agent-add-others" data-val="Walk-in" data-toggle="tooltip" data-placement="top" title="Walk-in"></i><br>
																							<label class="si-agent-label si-agent-add">Walk-in</label>
																						</div>
																					</div>
																					<div class="col-xs-4 col-sm-4 col-md-4 text-center">
																						<div class="si-agent" data-toggle="tooltip" data-placement="top" title="Search Database">
																							<i class="fa fa-search size-80 dark si-agent-search" data-toggle="collapse" data-target="#search-form" aria-expanded="false" aria-controls="search-form"></i><br>
																							<label class="si-agent-label">Search</label>
																						</div>
																					</div>
																					<div class="col-xs-12 details-div-dark"></div>
																					<div class="col-xs-12">
																						<div class="collapse" id="search-form">
																							<div class="input-group">
																								<span class="input-group-addon text-bold white bg-dark"><i class="fa fa-search"></i></span>
																								<input type="text" class="form-control si-agent-search-value" data-var="tours" placeholder="Agent Name">
																								<span class="input-group-btn">
																									<button class="btn btn-lg btn-dark-2 si-agent-search-button" type="button">Search</button>
																								</span>
																							</div>
																						</div>
																					</div>
																					<div class=" col-xs-12 content-lower"></div>
																					<div class="si-agent-search-results">
																						<div  style="overflow: auto;height: 220px; width: 550px;">
  																							<table class="table table-hover" style="width: 300px;" cellpadding="0" cellspacing="0">
																								<tbody>
																									@foreach($agents as $a)
																										@if($a->notes)
																											@php
																												$notes = $a->notes;
																											@endphp
																										@else
																											@php
																												$notes = "No notes for this agent";
																											@endphp
																										@endif
																										<tr>
																											<td class="bg-green text-center vertical-middle row-pad nature-border toggle-agent-details" data-agent-title="{{$a->id}}" data-agent-payment="{{$a->payment_terms}}" data-agent-tin="{{$a->id}}" data-agent-note="{{$notes}}" data-agent-contacts="{{$a->aCount}}">
																												<span class="white text-bold">{{$a->id}}</div></span>
																											</td>
																											<td class="bg-green text-center vertical-middle row-pad nature-border toggle-agent-details" data-agent-title="{{$a->name}}" data-agent-payment="{{$a->payment_terms}}" data-agent-tin="{{$a->tin}}" data-agent-note="{{$notes}}" data-agent-contacts="{{$a->aCount}}">
																												<span class="white text-bold">{{$a->nature}}</span>
																											</td>
																											<td class="text-center vertical-middle row-pad toggle-agent-details" data-agent-title="{{$a->name}}" data-agent-payment="{{$a->payment_terms}}" data-agent-tin="{{$a->tin}}" data-agent-note="{{$notes}}" data-agent-contacts="{{$a->aCount}}">
																												<span class="black text-bold size-m black"> {{$a->name}}</span><br>
																												<span class="black text-bold size-13"><i class="fa fa-map-marker blue-2"></i> {{$a->address}}</span>
																											</td>
																											<td class="text-center vertical-middle row-pad agent-add-button narture-border">
																												<a data-id="{{$a->id}}"  title="view details" data-placement="top" onclick="$('#agent_id').val($(this).data('id')); $('#agent-details-modal').modal('show');" ><i class='zmdi zmdi-arrow-right'></i></a>
																												<a data-id="{{$a->id}}"  title="add this agent" data-placement="top" onclick="$('#agent_id').val($(this).data('id')); $('#agent-add-modal').modal('show');" ><i class='zmdi zmdi-edit'></i></a>
																											</td>
																										</tr>

																									@endforeach
																								</tbody>
																							</table>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																</div>&nbsp;
																<div class="col-ms-3">
																	<div class="card">
																		<div class="card-header">
																			<strong>Particular</strong>
																		</div>
																		<div class="card-body card-block">
																			<div class="row">
																				<div class="col-xs-6 col-sm-6 col-md-6 text-center">
																					<div class="si-particular">
																						<i class="fa fa-pencil-square size-80 dark-blue particular-add-custom" data-toggle="tooltip" data-placement="top" title="Custom"></i><br>
																						<label class="si-agent-label si-particular-add" data-val="web">Custom</label>
																					</div>
																				</div>
																				<div class="col-xs-6 col-sm-6 col-md-6 text-center">
																					<div class="si-particular" data-toggle="tooltip" data-placement="top" title="Search Database">
																						<i class="fa fa-search size-80 dark si-particular-search" data-toggle="collapse" data-target="#search-particular" aria-expanded="false" aria-controls="search-particular"></i><br>
																						<label class="si-agent-label">Search</label>
																					</div>
																				</div>
																				<div class="col-xs-12 details-div-dark"></div>
																				<div class="col-xs-12">
																					<div class="collapse" id="search-particular">
																						<div class="input-group">
																							<span class="input-group-addon text-bold white bg-dark"><i class="fa fa-search"></i></span>
																							<input type="text" class="form-control si-particular-search-value" data-var="tours" placeholder="Tour Name">
																							<span class="input-group-btn">
																								<button class="btn btn-lg btn-dark-2 si-particular-search-button" type="button">Search</button>
																							</span>
																						</div>
																					</div>
																				</div>
																				<div class=" col-xs-12 content-lower"></div>
																				<div class="si-particular-search-results">
																					<div  style="overflow: auto;height: 245px; width: 550px;">
  																						<table class="table table-hover" style="width: 300px;" cellpadding="0" cellspacing="0">
																							<tbody>
																								@foreach($tours as $t)
																									<tr>
																										<td class="bg-blue-2 text-center vertical-middle row-pad nature-border toggle-particular-details">
																											<span class="white text-bold">{{$t->id}}</span>
																										</td>
																										<td class="bg-blue-2 text-center vertical-middle row-pad nature-border toggle-particular-details">
																											<span class="white text-bold">{{$t->code}}</span>
																										</td>
																										<td class="text-center vertical-middle row-pad toggle-particular-details">
																											<span class="black text-bold size-s black"> {{$t->name}}</span><br>
																											<span class="black text-bold size-13"><i class="fa fa-tags blue-2"></i> {{$t->type}}</span>
																										</td>
																										<td class="text-center vertical-middle row-pad agent-add-button narture-border">
																												<a data-id="{{$t->id}}"  title="view details" data-placement="top" onclick="$('#particular_id').val($(this).data('id')); $('#particular-details-modal').modal('show');" ><i class='zmdi zmdi-arrow-right'></i></a>
																												<a data-id="{{$t->id}}"  title="add this particular" data-placement="top" onclick="$('#particular_id').val($(this).data('id')); $('#particular-add-modal').modal('show');" ><i class='zmdi zmdi-edit'></i></a>
																										</td>
																									</tr>
																								@endforeach
																							</tbody>
																						</table>

																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
													</div>
												</div>
											</div>
											<div class="card-footer">
												<button type="submit" class="btn btn-lg btn-blue btn-block col-lg-12">
													<i class="fa fa-dot-circle-o"></i> Submit
												</button>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="copyright">
											<p>Copyright © 2021 Cebu Trip Tours. All rights reserved. Template by <a href="#">Cebu Trip Tours</a>.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div><!--END MAIN CONTENT-->

					<!-- agent add modal -->
					<div class="modal fade" id="agent-add-modal" tabindex="-1" role="dialog" aria-labelledby="addModal" >
								<div class="modal-dialog modal-lg" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="largeModalLabel">Agent Form</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											@foreach($sia as $s)
												<div class="mt-3">
													<div class="row form-group">
														<div class="col col-md-3">
															<label for="text-input" class=" form-control-label">Agent ID </label>
														</div>
														<div class="col-12 col-md-9">
															<input type="text" name="agent_id" id="agent_id"  value="" class="form-control" readonly>
															<small class="help-block form-text">Please enter a agent id</small>
														</div>
													</div>
													<div class="row form-group">
														<div class="col col-md-3">
															<label for="text-input" class=" form-control-label">Agent Name </label>
														</div>
														<div class="col-12 col-md-9">
															<input type="text" name="agent_name" id="agent_name" value="{{$s->agent_name}}" placeholder="Enter Agent Name" class="form-control {{ $errors->has('agent_name') ? 'is-invalid' : ''}}" require>
															<small class="help-block form-text">Please enter a agent name</small>
															@if($errors->has('agent_name'))
																<span class="has-error">{{ $errors->first('agent_name') }}</span>
															@endif
														</div>
													</div>
													<div class="row form-group">
														<div class="col col-md-3">
															<label for="text-input" class=" form-control-label">Agent Address </label>
														</div>
														<div class="col-12 col-md-9">
															<input type="text" name="agent_address" id="agent_address" value="{{$s->agent_address}}" placeholder="Enter Agent Address" class="form-control {{ $errors->has('agent_address') ? 'is-invalid' : ''}}" require>
															<small class="help-block form-text">Please enter a agent address</small>
															@if($errors->has('agent_address'))
																<span class="has-error">{{ $errors->first('agent_address') }}</span>
															@endif
														</div>
													</div>
													<div class="row form-group">
														<div class="col col-md-3">
															<label for="text-input" class=" form-control-label">Agent TIN </label>
														</div>
														<div class="col-12 col-md-9">
															<input type="text" name="agent_tin" id="agent_tin" value="{{$s->agent_tin}}" placeholder="Enter Agent TIN" class="form-control {{ $errors->has('agent_tin') ? 'is-invalid' : ''}}" require>
															<small class="help-block form-text">Please enter a agent tin</small>
															@if($errors->has('agent_tin'))
																<span class="has-error">{{ $errors->first('agent_tin') }}</span>
															@endif
														</div>
													</div>
													<div class="row form-group">
														<div class="col col-md-3">
															<label for="text-input" class=" form-control-label">Agent Contract </label>
														</div>
														<div class="col-12 col-md-9">
															<input type="text" name="agent_contract" id="agent_contract" value="{{$s->agent_contract}}" placeholder="Enter Agent Contract" class="form-control {{ $errors->has('agent_contract') ? 'is-invalid' : ''}}" require>
															<small class="help-block form-text">Please enter a agent contract</small>
															@if($errors->has('agent_contract'))
																<span class="has-error">{{ $errors->first('agent_contract') }}</span>
															@endif
														</div>
													</div>
													<div class="row form-group">
														<div class="col col-md-3">
															<label for="text-input" class=" form-control-label">Agent Payment </label>
														</div>
														<div class="col-12 col-md-9">
															<input type="text" name="agent_payment" id="agent_payment" value="{{$s->agent_payment}}" placeholder="Enter Agent Contract" class="form-control {{ $errors->has('agent_payment') ? 'is-invalid' : ''}}" require>
															<small class="help-block form-text">Please enter a agent payment</small>
															@if($errors->has('agent_payment'))
																<span class="has-error">{{ $errors->first('agent_payment') }}</span>
															@endif
														</div>
													</div>
													<div class="row form-group">
														<div class="col col-md-3">
															<label for="text-input" class=" form-control-label">Agent Note </label>
														</div>
														<div class="col-12 col-md-9">
															<input type="text" name="agent_note" id="agent_note" value="{{$s->agent_note}}" placeholder="Enter Agent Note" class="form-control {{ $errors->has('agent_note') ? 'is-invalid' : ''}}" require>
															<small class="help-block form-text">Please enter a agent note</small>
															@if($errors->has('agent_note'))
																<span class="has-error">{{ $errors->first('agent_note') }}</span>
															@endif
														</div>
													</div>
													<div class="row form-group">
														<div class="col col-md-3">
															<label for="text-input" class=" form-control-label">Agent Nature </label>
														</div>
														<div class="col-12 col-md-9">
															<input type="text" name="agent_nature" id="agent_nature" value="{{$s->agent_nature}}"placeholder="Enter Agent Nature" class="form-control {{ $errors->has('agent_nature') ? 'is-invalid' : ''}}" require>
															<small class="help-block form-text">Please enter a agent note</small>
															@if($errors->has('agent_nature'))
																<span class="has-error">{{ $errors->first('agent_nature') }}</span>
															@endif
														</div>
													</div>
												</div >
											@endforeach
										</div>
									</div>
								</div>
					</div><!-- end agent add modal-->

					<!-- agent details modal -->
					<div class="modal fade" id="agent-details-modal" tabindex="-1" role="dialog" aria-labelledby="agent-details-modal">
						<div class="modal-dialog  modal-lg" role="document">
							<div class="modal-content">
								<div class="modal-header bg-dark">
									<h3 class="modal-title modal-header-title text-center white" id="myModalLabel"><i class="fa fa-info-circle"></i> <span class="agent-details-title"></span> </h3>
								</div>
								<div class="modal-body">
									<div class="row">
										<div class="col-xs-12 col-sm-12 text-center">
											<i class="fa fa-id-card-o"></i> <b>TIN:</b> <span class="agent-details-tin text-bold black"></span>
										</div>
										<div class="visible-xs visible-sm hidden-md hidden-lg content-lower-5"></div>
										<div class="col-xs-12 col-sm-12 text-center">
											<i class="fa fa-phone-square"></i> <b>Contacts:</b> <span class="agent-details-contacts text-bold black"></span> <b class="black">contacts</b>
										</div>
										<div class="visible-xs visible-sm hidden-md hidden-lg content-lower-5"></div>
										<div class="col-xs-12 col-sm-12 text-center">
											<i class="fa fa-money"></i> <b>Payment Terms:</b> <span class="agent-details-payment text-bold black"></span>
										</div>
										<div class="col-xs-12 details-div-4"></div>
										<div class="col-xs-12 text-center">
											<span class="agent-details-note text-bold black"></span>
										</div></div>
									</div>
								</div>
							</div>
						</div>
					</div><!-- end agent details modal -->

					<!-- particular add modal -->
					<div class="modal fade" id="particular-add-modal" tabindex="-1" role="dialog" aria-labelledby="particular-add-modal">
						<div class="modal-dialog  modal-lg" role="document">
							<div class="modal-content">
								<div class="modal-header bg-dark">
									<h3 class="modal-title modal-header-title text-center white" id="myModalLabel"><i class="fa fa-info-circle"></i> <span class="agent-details-title"></span> </h3>
								</div>
								<div class="modal-body">
									@for($c=0;$c<=0;$c++)
										@foreach($sip as $p)
											<div class="row form-group">
												<div class="col col-md-3">
													<label for="text-input" class=" form-control-label">Particular Booking Reference </label>
												</div>
												<div class="col-12 col-md-9">
													<input type="text" name="particular_booking_ref[]" id="particular_booking_ref[]" value="{{$p->booking_reference}}" placeholder="Enter Particular Booking Reference " class="form-control {{ $errors->has('particular_booking_ref.'.$c) ? 'is-invalid' : ''}}" require>
													<small class="help-block form-text">Please enter a particular booking reference</small>
													@if($errors->has('particular_booking_ref.'.$c))
														<span class="has-error">{{ $errors->first('particular_booking_ref.'.$c) }}</span>
													@endif
												</div>
											</div>
											<div class="row form-group">
												<div class="col col-md-3">
													<label for="text-input" class=" form-control-label">Particular Name </label>
												</div>
												<div class="col-12 col-md-9">
													<input type="text" name="particular_name[]" id="particular_name[]" value="{{$p->particular}}" placeholder="Enter Particular Name" class="form-control {{ $errors->has('particular_name.'.$c) ? 'is-invalid' : ''}}" require>
													<small class="help-block form-text">Please enter a particular name</small>
													@if($errors->has('particular_name.'.$c))
														<span class="has-error">{{ $errors->first('particular_name.'.$c) }}</span>
													@endif
												</div>
											</div>
											<div class="row form-group">
												<div class="col col-md-3">
													<label for="date-input" class=" form-control-label">Particular Date </label>
												</div>
												<div class="col-12 col-md-9">
													<input type="date"  name="particular_date[]" id="particular_date[]" value="{{$p->tour_date}}" placeholder="Enter Particular Date" class="form-control {{ $errors->has('particular_date.'.$c) ? 'is-invalid' : ''}}" require>
													<small class="help-block form-text">Please enter a particular date</small>
													@if($errors->has('particular_date.'.$c))
														<span class="has-error">{{ $errors->first('particular_date.'.$c) }}</span>
													@endif
												</div>
											</div>
											<div class="row form-group">
												<div class="col col-md-3">
													<label for="number-input" class=" form-control-label">Particular Rate  </label>
												</div>
												<div class="col-12 col-md-9">
													<input type="number" name="particular_rate[]" id="particular_rate[]" value="$p->rate" min="1" placeholder="Enter Particular Rate" class="form-control {{ $errors->has('particular_rate.'.$c) ? 'is-invalid' : ''}}" require>
													<small class="help-block form-text">Please enter a Particular Rate</small>
													@if($errors->has('particular_rate.'.$c))
														<span class="has-error">{{ $errors->first('particular_rate.'.$c) }}</span>
													@endif
												</div>
											</div>
											<div class="row form-group">
												<div class="col col-md-3">
													<label for="number-input" class=" form-control-label">Particular Commission </label>
												</div>
												<div class="col-12 col-md-9">
													<input type="number" name="particular_com[]" id="particular_com[]" value="{{$p->commission}} placeholder="Enter Particular Commission" class="form-control {{ $errors->has('particular_com.'.$c) ? 'is-invalid' : ''}}" require>
													<small class="help-block form-text">Please enter a particular commission</small>
													@if($errors->has('particular_com.'.$c))
														<span class="has-error">{{ $errors->first('particular_com.'.$c) }}</span>
													@endif
												</div>
											</div>
											<div class="row form-group">
												<div class="col col-md-3">
													<label for="number-input" class=" form-control-label">Particular Local</label>
												</div>
												<div class="col-12 col-md-9">
													<input type="number" name="particular_local[]" id="particular_local[]" value="{{$p->pax}}" placeholder="Enter Particular Local" class="form-control {{ $errors->has('particular_local.'.$c) ? 'is-invalid' : ''}}" require>
													<small class="help-block form-text">Please enter a particular local</small>
													@if($errors->has('particular_local.'.$c))
														<span class="has-error">{{ $errors->first('particular_local.'.$c) }}</span>
													@endif
												</div>
											</div>
											<div class="form-group form-group-reset">
												<div class="input-group">
													<div class="input-group-addon">TOTAL</div>
													<input type="text" class="form-40 particular_total border-radius-0 read-only" value="{{number_format($p->total,2,'.','')}}" placeholder="Total" name="particular_total[]" value="0.00" readonly>
													<input type="hidden" name="particular_type[]" value="{{$p->particular_type}}">
													<input type="hidden" name="particular_guide[]" value="{{$p->paticular_guide}}">
													<input type="hidden" name="particular_id[]" value="{{$p->particular_id}}">
													<input type="hidden" name="particular_foreign[]" value="{{$p->foreign_pax}}">
												</div>
											</div>
										@endforeach
									@endfor
								</div>
							</div>
						</div>
					</div><!-- end particular add  modal -->

					<!-- particular details modal -->
					<div class="modal fade" id="particular-details-modal" tabindex="-1" role="dialog" aria-labelledby="particular-details-modal">
						<div class="modal-dialog  modal-lg" role="document">
							<div class="modal-content">
								<div class="modal-header bg-dark">
									<h3 class="modal-title modal-header-title text-center white" id="myModalLabel"><i class="fa fa-info-circle"></i> <span class="particular-details-title"></span> </h3>
								</div>
								<div class="modal-body">
									<div class="row">
										<div class="col-xs-12 col-sm-12 text-center">
											<i class="fa fa-id-card-o"></i> <b>Total Price:</b> <span class="particular-details-total text-bold black"></span>
										</div>
										<div class="visible-xs visible-sm hidden-md hidden-lg content-lower-5"></div>
										<div class="col-xs-12 col-sm-12 text-center">
											<i class="fa fa-phone-square"></i> <b>Total Commission:</b> <span class="particular-details-com text-bold black"></span>
										</div>
										<div class="col-xs-12 details-div-4"></div>
										<div class="col-xs-12 text-center">
											<i class="fa fa-money"></i> <b>Net Amount:</b> <span class="particular-details-net text-bold black"></span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div><!-- end particular details  modal -->
				</form>
			</div>	<!-- END PAGE CONTAINER-->

</x-app-layout>
