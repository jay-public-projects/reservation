<div class="dashboard">
    <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
        <h5 class="my-0 mr-md-auto font-weight-normal">
            <img src="{{ asset('../img/logo.png') }}" alt="Cebu Trip Tours" class="logo"/>
        </h5>
        <form method="POST" action="{{ route('logout') }}">
            @csrf
            <x-jet-dropdown-link class="btn btn-white" href="{{ route('logout') }}"
                                 onclick="event.preventDefault();
								 this.closest('form').submit();">
                LOGOUT
            </x-jet-dropdown-link>
        </form>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="card-box">
                    <div class="card-title">
                        <h2><i class="fa fa-btn fa-folder-open fa-navbar"></i> Sales Invoice</h2>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda, </p>
                    </div>
                    <div class="card-link">
                        <a href="{{url('reservation/salesinvoice')}}" class="c-link">View More
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card-box">
                    <div class="card-title">
                        <h2><i class="fas fa-plane"></i> Tours</h2>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda, </p>
                    </div>
                    <div class="card-link">
                        <a href="{{url('reservation/tours')}}" class="c-link">View More
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card-box">
                    <div class="card-title">
                        <h2><i class="fas fa-user-circle"></i> Agents</h2>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda, </p>
                    </div>
                    <div class="card-link">
                        <a href="{{url('reservation/agents')}}" class="c-link">View More
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
