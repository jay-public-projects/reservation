<x-app-layout>


  		<!-- MENU SIDEBAR-->
      @include('pages.side_layout.admin_side_layout')

        <!-- PAGE CONTAINER-->
        <div class="page-container"><!-- HEADER DESKTOP-->
            <header class="header-desktop">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            <div class="title-3 text-uppercase">
                                <h3>Reservation Page</h3>
                            </div>
                            <div class="header-button">
                                <div class="account-wrap">
                                    <div class="account-item clearfix js-item-menu">
                                        @include('navigation-menu')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- HEADER DESKTOP-->
            <x-slot name="header">
              <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                  {{ __('View Tour | Reservation') }}
              </h2>
          </x-slot>
          <form action="{{ url('admin/tour/edit') }}" method="post">
            <!-- MAIN CONTENT-->
            <div class="main-content">
              <div class="section__content section__content--p30">
                <div class="container-fluid">
                  <div class="row">
									  <div class="col-lg-12">
										  <div class="card">
                        @foreach($tour as $t)
                          <div class="card-header">View Tour</div>
                            <div class="card-body">
                              <div class="row">
                                <div class="col-lg-6">
                                    <div class="card">
                                      <div class="card-header">
                                          <strong> View Tour Package Form</strong>
                                      </div>
                                      <div class="card-body card-block">
                                        <div class="row form-group">
                                          <div class="col col-md-3">
                                            <label for="text-input" class=" form-control-label"> Tour Name </label>
                                          </div>
                                          <div class="col-12 col-md-9">
                                            <input type="text" id="name" name="name" value="{{ $t->name }}" placeholder="Enter Tour Name" class="form-control" readonly>
                                            <small class="form-text text-muted">Please enter your tour Name</small>
                                          </div>
                                        </div>
                                        <div class="row form-group">
                                          <div class="col col-md-3">
                                            <label for="text-input" class=" form-control-label">Tour Code </label>
                                          </div>
                                          <div class="col-12 col-md-9">
                                            <input type="text" id="code" name="code" value="{{ $t->code }}" placeholder="Enter Tour Code" class="form-control" readonly>
                                            <small class="help-block form-text">Please enter your tour code</small>
                                          </div>
                                        </div>
                                        <div class="row form-group">
                                          <div class="col col-md-3">
                                            <label for="select" class=" form-control-label">Tour Type</label>
                                          </div>
                                          <div class="col-12 col-md-9">
                                            <select name="type" id="type" class="form-control" readonly>
                                              <option <?php if($t->type === "Shared")echo "selected";?> value="1" >Shared Tour</option>
                                              <option <?php if($t->type === "Private")echo "selected";?> value="2">Private Tour</option>
                                            </select>
                                          </div>
                                        </div>
                                        <div class="row form-group">
                                          <div class="col col-md-3">
                                            <label for="number-input" class=" form-control-label">Tour Duration </label>
                                          </div>
                                          <div class="col-12 col-md-9">
                                            <input type="number" id="duration" name="duration" min="1" value="{{ $t->duration }}" placeholder="Enter Tour Duration" class="form-control" readonly>
                                            <small class="help-block form-text">Please enter your tour duration</small>
                                          </div>
                                        </div>
                                        <div class="row form-group">
                                          <div class="col col-md-3">
                                            <label for="number-input" class=" form-control-label">Lead Time </label>
                                          </div>
                                          <div class="col-12 col-md-9">
                                            <input type="number" id="lead_time" name="lead_time" min="0" value="{{ $t->lead_time }}" placeholder="Enter lead time" class="form-control" readonly>
                                            <small class="form-text text-muted">Please enter your lead time</small>
                                          </div>
                                        </div>
                                        <div class="row form-group">
                                          <div class="col col-md-3">
                                            <label for="number-input" class=" form-control-label">Minimum Pax </label>
                                          </div>
                                          <div class="col-12 col-md-9">
                                            <input type="number" id="min_pax" name="min_pax" value="{{ $t->min_pax }}" placeholder="Enter Minimum Pax" class="form-control" readonly>
                                            <small class="help-block form-text">Please enter your minimum pax</small>
                                          </div>
                                        </div>
                                        <div class="row form-group">
                                          <div class="col col-md-3">
                                            <label for="textarea-input" class=" form-control-label">Description </label>
                                          </div>
                                          <div class="col-12 col-md-9">
                                            <textarea name="description" id="description" rows="4" placeholder="Content..." class="form-control" readonly>{{ $t->description }}</textarea>
                                          </div>
                                        </div>
                                        <div class="row form-group">
                                          <div class="col col-md-3">
                                            <label for="textarea-input" class=" form-control-label">Pick Up </label>
                                          </div>
                                          <div class="col-12 col-md-9">
                                            <textarea name="pickup" id="pickup" rows="4" placeholder="Content..." class="form-control" readonly>{{ $t->pick_up }}</textarea>
                                          </div>
                                        </div>
                                        <div class="row form-group">
                                          <div class="col col-md-3">
                                            <label for="textarea-input" class=" form-control-label">Highlights </label>
                                          </div>
                                          <div class="col-12 col-md-9">
                                            <textarea name="highlights" id="highlights" rows="4" placeholder="Content..." class="form-control" readonly>{{ $t->highlights }}</textarea>
                                          </div>
                                        </div>
                                        <div class="row form-group">
                                          <div class="col col-md-3">
                                            <label for="textarea-input" class=" form-control-label">Inclusions </label>
                                          </div>
                                          <div class="col-12 col-md-9">
                                            <textarea name="inclusions" id="inclusions" rows="4"  placeholder="Content..." class="form-control" readonly>{{ $t->inclusions }}</textarea>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                  <div class="card">
                                    <div class="card-header">
                                      <strong> WITH GUIDE</strong>
                                    </div>
                                    <div class="card-body card-block">
                                      @foreach($tour_pax as $tp)
                                        <div class="form-group">
                                          <div class="input-group">
                                            <div class="input-group-addon">{{$tp->id}} PAX</div>
                                            <input type="number" id="w_amount[]" name="w_amount[]" min="100" value="{{$tp->with_guide}}" placeholder="Price" class="form-control"readonly>
                                            <input type="hidden" name="w_pax[]" value="{{$tp->pax}}">
                                            <input type="hidden" name="w_guide[]" value="{{ $tp->with_guide }}">
                                            <input type="hidden" name="pax_id[]" value="{{ $tp->id}}">
                                          </div>
                                        </div>	&nbsp;
                                      @endforeach
                                    </div>
                                  </div>
                                </div>
                                <div class="col-lg-3">
                                  <div class="card">
                                    <div class="card-header">&nbsp;
                                      <strong> WITHOUT GUIDE</strong>
                                    </div>
                                    <div class="card-body card-block">
                                      @foreach($tour_pax as $tp)
                                        <div class="form-group">
                                          <div class="input-group">
                                            <div class="input-group-addon">{{$tp->pax}} PAX</div>
                                            <input type="number" id="w_amount[]" name="wo_amount[]" min="100" value="{{$tp->without_guide}}" placeholder="Price" class="form-control"readonly>
                                            <input type="hidden" name="wo_pax[]" value="{{$tp->pax}}">
                                            <input type="hidden" name="wo_guide[]" value="{{ $tp->without_guide }}">
                                            <input type="hidden" name="pax_id[]" value="{{ $tp->id}}">
                                          </div>
                                        </div>	&nbsp;
                                      @endforeach
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-lg-6">
                                  <div class="form-group">
                                    <div class="input-group">
                                      <div class="input-group-addon">Foreign Rate Additional</div>
                                        <input type="number" class="form-control" min="0" value="0" name="foreign_rate" placeholder="Price" value="{{$t->foreign_rate}}" readonly>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-lg-6">
                                    <button type="button" class="btn btn-blue col-lg-12" data-toggle="modal" data-target="#peakdate">
                                      <i class="zmdi zmdi-plus"></i>Peak Date
                                    </button>
                                  </div>
                                </div>
                              </div>
                              <div class="col-xs-12">
                                  <button type="submit" class="btn btn-blue btn-lg btn-submit col-lg-12"><i class="fa fa-save"></i> Commit</button>
                              </div>
                            </div>
                          </div>
                        @endforeach
										  </div>
									  </div>
								  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="copyright">
                        <p>Copyright © 2021 CebuTripTours. All rights reserved. Template by <a href="#">Cebu Trip Tours</a>.</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- modal medium -->
            <div class="modal fade" id="peakdate" tabindex="-1" role="dialog" aria-labelledby="peakdate" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="peakdate">Medium Modal</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    @foreach($tour_peak as $tpk)
                      <div class="row form-group">
                        <div class="col col-md-3">
                          <label for="number-input" class=" form-control-label">Date From  </label>
                        </div>
                        <div class="col-12 col-md-9">
                          <input type="date" name="peak_from[]" placeholder="Date From"class="form-control"value="{{$tpk->from}}" readonly>
                          <small class="form-text text-muted">Please enter the date from</small>
                        </div>
                      </div>
                      <div class="row form-group">
                        <div class="col col-md-3">
                          <label for="number-input" class=" form-control-label">Date To  </label>
                        </div>
                        <div class="col-12 col-md-9">
                          <input type="date" name="peak_to[]"  placeholder="Date To" class="form-control" value="{{$tpk->to}}" readonly>
                          <small class="form-text text-muted">Please enter the date to</small>
                        </div>
                      </div>
                      <div class="row form-group">
                        <div class="col col-md-3">
                          <label for="select" class=" form-control-label">Amount Type</label>
                        </div>
                        <div class="col-12 col-md-9">
                          <select id="peak_type" name="peak_type[]"class="form-control" readonly>
                            <option class="option-style" value="">AMOUNT TYPE</option>
                            <option class="option-style" <?php if($tpk->type === "amount"){echo "selected";} ?> value="amount">Amount</option>
                            <option class="option-style" <?php if($tpk->type === "percent"){echo "selected";} ?> value="percent">Percentage</option>
                          </select>
                        </div>
                      </div>
                      <input type="hidden" name="peak_id[]" value="0">
                      <div class="form-group">
                        <div class="col col-md-3">
                          <label for="number-input" class=" form-control-label">Amount</label>
                        </div>
                        <div class="col-12 col-md-9">
                          <input type="number" name="peak_amount[]"  placeholder="Enter Amount" class="form-control" value="$tpk->peak_amount" readonly>
                          <small class="form-text text-muted">Please enter the amount</small>
                        </div>
                      </div>
                    @endforeach
                  </div>
                </div>
              </div>
            </div>
            <!-- end modal medium -->
          </form>
        </div>
      </div>
  </x-app-layout>
