<x-app-layout>
	<!-- MENU SIDEBAR-->
	@include('pages.side_layout.reservation_side_layout')
		<!-- PAGE CONTAINER-->
		<div class="page-container">
            <!-- HEADER DESKTOP-->
            <header class="header-desktop">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            <div class="title-3 text-uppercase">
                                <h3>Reservation Page</h3>
                            </div>
                            <div class="header-button">
                                <div class="account-wrap">
                                    <div class="account-item clearfix js-item-menu">
                                        @include('navigation-menu')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- HEADER DESKTOP-->
			<x-slot name="header">
				<h2 class="font-semibold text-xl text-gray-800 leading-tight">
					{{ __('Tours | Reservation') }}
				</h2>
			</x-slot>
			<!-- MAIN CONTENT-->
			<div class="main-content">
				<div class="section__content section__content--p30">
					<div class="container-fluid">
						<div class="row">
							<div class="col-lg-12">
								<div class="card">
									<div class="card-header">View Tours</div>
										<div class="card-body">
											<div class="row">
												<div class="col-lg-3">
													<button type="button" class="btn btn-blue-2 btn-lg dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
														Code <span class="caret"></span>
													</button>
													<ul class="dropdown-menu">
														<li role="separator" class="divider"></li>
														@foreach($codes as $code)
															<li class="text-center"><a href="#" class="tour-code filter" data-act="filter" data-filtval="{{$code}}">{{$code}}</a></li>
															<li role="separator" class="divider"></li>
														@endforeach
													</ul>
												</div>
												<div class="col-lg-6">
													<form class="form-header" action="" method="POST">
														<input class="au-input au-input--xl" type="text" name="search" data-var="tour" placeholder="Search Tours" />
														<button class="au-btn--submit" type="submit" data-act="search">
															<i class="zmdi zmdi-search"></i>
														</button>
													</form>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">&nbsp;
													<div class="table-responsive table-responsive-data2">
														<table class="table table-data2">
															<thead>
																<tr>
																	<th>Name</th>
																	<th>Tour</th>
																	<th>code</th>
																	<th>status</th>
																</tr>
															</thead>
															<tbody>
																@foreach($tours as $t)
																	<tr class="tr-shadow">
																		<td>{{$t->name}}</td>
																		<td>{{$t->type}}</td>
																		<td>{{$t->code}} </td>
																		<td>
																			<div class="table-data-feature">
																				<button type="button" class="dropdown-toggle gear-setting" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																					<i class="fa fa-gear white size-s"></i>
																				</button>
																				<ul class="dropdown-menu">
																					<li class="text-center"><a href="{{url('admin/edit/tour').'/'.$t->id}}" class="text-bold"><i class="fa fa-pencil-square-o blue"></i> Edit</a></li>
																					<li role="separator" class="divider"></li>
																					<li class="text-center"><a href="#" class="text-bold deleteThisAgent" data-act="{{$t->id}}" data-var="tours" data-name="{{$t->name}}"><i class="fa fa-trash red"></i> Delete</a></li>
																				</ul>
																			</div>
																		</td>
																	</tr>
																@endforeach
															</tbody>
														</table>
													</div><!-- END DATA TABLE -->
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="copyright">
									<p>Copyright © 2021 Cebu Trip Tours. All rights reserved. Template by <a href="#">Cebu Trip Tours</a>.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</x-app-layout>
