<x-app-layout>
    @if(auth()->user()->level == 0)
        @include('pages.admin.admin_dashboard')
    @elseif(auth()->user()->level == 1)
        @include('pages.reservation.reservation_dashboard')
    @else
        @include('pages.accounting.accounting_dashboard')
    @endif

    <script src="/js/socket.io.js"></script>
    <script>
            $(document).ready(function(){

                var socket = io('{{ Request::root() }}:3000');

            });
    </script>
</x-app-layout>
